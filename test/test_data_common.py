from src.data.common import *

from . import *


class TestAddMissingDates:
    def test_ok(self):
        inp = pd.DataFrame({'store': [1, 1, 2, ],
                            'dept': [1, 1, 1],
                            'date': ['2020-01-01', '2020-01-02', '2020-01-03']})
        inp['date'] = pd.to_datetime(inp['date'])

        out = (add_missing_dates(inp, groups=['store', 'dept'], col_date='date')
               .sort_values(['store', 'dept', 'date']))
        exp = pd.DataFrame({'store': [1, 1, 1, 2, 2, 2],
                            'dept': [1, 1, 1, 1, 1, 1],
                            'date': ['2020-01-01', '2020-01-02', '2020-01-03',
                                     '2020-01-01', '2020-01-02', '2020-01-03']})
        exp['date'] = pd.to_datetime(exp['date'])

        for c in ['date', 'store', 'dept']:
            np.testing.assert_array_equal(out[c], exp[c])
