import datetime
import json
from pathlib import Path

import pandas as pd
import pytest
from src.app import DB
from src.app.models import save_multiple
from src.data.presse.models import RSSContent, RSSFeed

from . import TESTDIR


@pytest.fixture(scope="module")
def raw_feed_content():
    fn = TESTDIR / "data" / "test_presse_feed_content.json"
    with fn.open() as f:
        content = json.load(f)
    return content


def test_insert_duplicate_does_nothing(db):
    feed0 = RSSFeed(name="test_feed0", url="feed_url")
    feed1 = RSSFeed(name="test_feed1", url="feed_url")
    save_multiple([feed0, feed1])

    feeds = RSSFeed.query.all()
    assert len(feeds) == 1

    first = feeds[0]
    assert first.id == 1
    assert first.name == "test_feed0"
    assert first.url == "feed_url"


def test_add_from_csv(db):
    fn = TESTDIR / "data" / "test_rss_feeds.csv"
    RSSFeed.add_from_csv(fn, sep=";")
    feeds = RSSFeed.query.all()
    assert len(feeds) == 3

    first = feeds[0]
    assert first.id == 1
    assert first.name == "À l'Ouest : Rouen"
    assert first.url == "https://a-louest.info/spip.php?page=backend"


def test_insert_raw_feed_content(db, raw_feed_content):

    RSSContent.insert_raw(raw_feed_content)

    entries = RSSContent.query.all()
    assert len(entries) == 20

    first = entries[0]
    assert first.feed_id is None
    assert first.fetch_dt == pd.Timestamp.utcnow().date()
    assert (
        first.url
        == "https://www.vosgesmatin.fr/sante/2021/04/10/covid-19-le-premier-ministre-en-deplacement-a-lyon-ce-samedi"
    )


def test_insert_raw_with_fetch_id_and_feed(db, raw_feed_content):
    feed = RSSFeed(name="test_feed", url="test_url")
    DB.session.add(feed)
    DB.session.commit()

    fetch_dt = pd.Timestamp("2020-01-01").date()

    RSSContent.insert_raw(raw_feed_content, feed=feed, fetch_dt=fetch_dt)

    entries = RSSContent.query.all()
    assert len(entries) == 20

    first = entries[0]
    assert first.feed_id == 1
    assert first.fetch_dt == fetch_dt


def test_insert_raw_with_duplicates(db, raw_feed_content):
    RSSContent.insert_raw(raw_feed_content)

    feed = RSSFeed(name="test_feed", url="test_url")
    save_multiple([feed])
    fetch_dt = pd.Timestamp("2020-01-01").date()
    RSSContent.insert_raw(raw_feed_content, feed=feed, fetch_dt=fetch_dt)

    entries = RSSContent.query.all()
    assert len(entries) == 20


def test_rsscontent_from_entry_no_link(db):
    entry = {"hello": "world"}
    fetch_dt = pd.Timestamp("2020-01-01").date()

    out = RSSContent.from_entry(entry, fetch_dt=fetch_dt)

    assert out.url == "47b634ff6cafada8f5606c3c7be8dceffa3608c54b44e7626e0e59d4"
    assert out.fetch_dt == fetch_dt
    assert out.content == '{"hello": "world"}'


def test_parse_simple_content(db, raw_feed_content):
    RSSContent.insert_raw(raw_feed_content)

    entry = RSSContent.query.first()
    url = entry.url
    entry.parse()
    assert entry.title.startswith('Covid-19 : "Il faut lever')
    assert entry.summary.startswith("Plus de 5 750 patients en réanimation,")
    assert entry.dt == datetime.date(2021, 4, 10)

    db.session.add(entry)
    db.session.commit()

    entry2 = RSSContent.query.get(url)
    assert entry2.title.startswith('Covid-19 : "Il faut lever')
    assert entry2.summary.startswith("Plus de 5 750 patients en réanimation,")
    assert entry2.dt == datetime.date(2021, 4, 10)


def test_parse_manual():
    content = RSSContent(url="azert", content='{"text":"hello world"}')
    content.parse()
    assert content.title is None
    assert content.dt is None
    assert content.summary == "hello world"


# def test_parse_updated():
#     fn = (
#         Path(__file__).parent
#         / "data"
#         / "presse"
#         / "fixtures"
#         / "test_article_updated_dt.json"
#     )
#     content = RSSContent(url="http://test_url", content=fn.open().read()).parse()
#
#     assert (
#         content.title == "Enquêter et agir contre l'intoxication de Rouen (et du monde)"
#     )
#     assert content.summary.startswith("<p>En soutien à la mobilisation nationale")
#     assert content.dt == date(2020, 9, 21)

#
# def test_parse_date_error():
#     fn = (
#         Path(__file__).parent
#         / "data"
#         / "presse"
#         / "fixtures"
#         / "test_parse_error_date.json"
#     )
#     content = RSSContent(url="http://test_url", content=fn.open().read()).parse()
#
#     assert content.title == "Massages assis et habillés"
#     assert content.summary.startswith("Vous êtes stressée ? Vous avez mal au dos ?")
#     assert content.dt == date(2020, 10, 27)
