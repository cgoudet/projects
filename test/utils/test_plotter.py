from src.utils.plotter import *

from .. import *


class TestPlotter:
    def setup_class(self):
        self.prefix = tempfile.mkdtemp()

    def teardown_class(self):
        shutil.rmtree(self.prefix)

    def test_simple_bar_annot_forced_colors(self):
        labels = ["first", "second", "third"]
        values = [1, 2, 3]
        colors = ["red", "orange", "green"]

        plotter = Plotter(kind="bar", annot=True, annot_pct=False)
        name = "simple_bar_annot_forced_colors"
        plotter.plot(
            values,
            labels,
            colors=colors,
            savename=os.path.join(self.prefix, name + ".png"),
            title=name,
            xlabel="xlabel",
            ylabel="ylabel",
        )

    def test_simple_bar_annot_pct_top1_colors(self):
        labels = ["first", "second", "third"]
        values = [1, 2, 3]
        colors = ["red", "orange", "green"]

        plotter = Plotter(kind="bar", annot=True, annot_pct=True, topn=1)
        name = "simple_bar_annot_pct_top1_colors"
        plotter.plot(
            values,
            labels,
            colors=colors,
            savename=os.path.join(self.prefix, name + ".png"),
            title=name,
        )

    def test_simple_barh_annot_top06_default_col(self):
        labels = ["first", "second", "third"]
        values = [1, 2, 3]

        plotter = Plotter(kind="barh", annot=True, annot_pct=True, topn=0.6)
        name = "test_simple_barh_annot_top06_default_col"
        plotter.plot(
            values,
            labels,
            savename=os.path.join(self.prefix, name + ".png"),
            title=name,
        )

    def test_hist_annot_top2(self):
        values = []
        for i in range(6):
            values += [i] * i

        plotter = Plotter(kind="hist", annot=True, annot_pct=True, topn=2)
        name = "hist_annot_top2"
        plotter.plot(
            values,
            bins=np.linspace(-0.5, 5.5, 7),
            savename=os.path.join(self.prefix, name + ".png"),
            title=name,
        )

    def test_hist_user_color(self):
        values = []
        for i in range(6):
            values += [i] * i

        plotter = Plotter(kind="hist", annot=True, annot_pct=True)
        name = "hist_user_color"
        plotter.plot(
            values,
            bins=np.linspace(-0.5, 5.5, 7),
            colors=["red", "red", "orange", "orange", "green", "green"],
            savename=os.path.join(self.prefix, name + ".png"),
            title=name,
        )

    def test_hist_user_color_bins_init(self):
        values = []
        for i in range(6):
            values += [i] * i

        plotter = Plotter(
            kind="hist",
            annot=True,
            annot_pct=True,
            colors=["red", "red", "orange", "orange", "green", "green"],
            bins=np.linspace(-0.5, 5.5, 7),
        )
        name = "hist_user_color_bins_init"
        plotter.plot(
            values, savename=os.path.join(self.prefix, name + ".png"), title=name
        )

    def test_hist_group_color(self):
        values = []
        for i in range(6):
            values += [i] * i

        plotter = Plotter(
            kind="hist",
            annot=True,
            annot_pct=True,
            colors=["red", "orange", "green"],
            group=[0, 1, 2, 0, 1, 2],
            bins=np.linspace(-0.5, 5.5, 7),
        )
        name = "hist_group_color"
        plotter.plot(
            values, savename=os.path.join(self.prefix, name + ".png"), title=name
        )

    def test_hist_group_color_topn(self):
        values = []
        for i in range(6):
            values += [i] * i

        plotter = Plotter(
            kind="hist",
            annot=True,
            annot_pct=True,
            colors=["red", "orange", "green"],
            group=[0, 1, 2, 0, 1, 2],
            topn=1,
            bins=np.linspace(-0.5, 5.5, 7),
        )
        name = "hist_group_color_topn"
        plotter.plot(
            values, savename=os.path.join(self.prefix, name + ".png"), title=name
        )


class TestTopn:
    def test_empty(self):
        assert not topn([], 4)

    def test_int_rank(self):
        a = np.array([6, 2, 1, 7, 4])
        out = topn(a, 2)
        assert list(out) == [1, 0, 0, 1, 0]

    def test_float_rank(self):
        a = np.array([6, 2, 1, 7, 4])
        out = topn(a, 0.83)
        assert list(out) == [1, 0, 0, 1, 1]


@pytest.mark.skip(reason="no way of currently testing this")
class TestMultiPlot:
    def setUp(self):
        fn = pathlib.Path(__file__).parent / ".." / "data" / "tests" / "titanic.csv"
        self.data = pd.read_csv(fn)
        self.prefix = pathlib.Path(__file__).parent / ".." / "tmp_test_plotter"
        self.prefix.mkdir(exist_ok=True)

    def test_float_float(self):
        plotter = MultiPlotter()
        self.data["rand"] = np.random.rand(len(self.data))
        plotter.plot(
            self.data,
            "Fare",
            "rand",
            self.prefix / "multiplot_float_float.png",
            title="Fare_rand",
        )

    def test_float_float_with_1value(self):
        plotter = MultiPlotter()
        self.data["rand"] = 1.2
        plotter.plot(
            self.data,
            "rand",
            "Fare",
            self.prefix / "multiplot_float_floatsingle.png",
            title="Fare_1",
        )

    def test_intcat_float(self):
        plotter = MultiPlotter()
        plotter.plot(
            self.data,
            "Pclass",
            "Fare",
            self.prefix / "multiplot_cat_float.png",
            title="Pclass_Fare",
        )

    def test_binary_float(self):
        plotter = MultiPlotter()
        plotter.plot(
            self.data,
            "Survived",
            "Fare",
            self.prefix / "multiplot_binary_float.png",
            title="Survived_Fare",
        )

    def test_cat_cat(self):
        plotter = MultiPlotter()
        plotter.plot(
            self.data, "Pclass", "Embarked", self.prefix / "multiplot_cat_cat.png"
        )

    def test_1D_float(self):
        plotter = MultiPlotter()
        plotter.plot(self.data, "Fare", None, self.prefix / "multiplot_1d_float.png")

    def test_1D_cat(self):
        plotter = MultiPlotter()
        plotter.plot(self.data, "Pclass", None, self.prefix / "multiplot_1d_cat.png")

    def test_1D_time(self):
        serie = pd.Series(pd.date_range(start="1/1/2018", periods=500, freq="H"))
        serie.name = "timestamp"
        serie = serie.to_frame()
        plotter = MultiPlotter()
        plotter.plot(serie, "timestamp", None, self.prefix / "multiplot_1d_time.png")
