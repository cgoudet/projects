from datetime import date

from sklearn.ensemble import RandomForestClassifier
from src import *

from .. import *


class TestutilsAddl:
    def test_standard(self):
        l1 = ["a", "d", "e"]
        l2 = ["b", "f", "c"]
        assert addl(l1, l2) == list("abcdef")

    def test_l2empty(self):
        l1 = ["a", "d", "e"]
        assert addl(l1, []) == list("ade")

    def test_l1empty(self):
        l2 = ["a", "z", "e"]
        assert addl([], l2) == list("aez")

    def test_both_empty(self):
        assert not addl([], [])


class TestDiffL:
    def test_no_common(self):
        l1 = ["a", "z", "e"]
        l2 = ["b", "f", "c"]
        assert diffl(l1, l2) == list("aez")

    def test_all_common(self):
        l1 = ["a", "z", "e"]
        l2 = ["z", "e", "a"]
        assert not diffl(l1, l2)

    def test_some_common(self):
        l1 = ["a", "z", "e"]
        l2 = ["z", "b", "a"]
        assert diffl(l1, l2) == ["e"]

    def test_l2empty(self):
        l1 = ["a", "z", "e"]
        assert diffl(l1, []) == list("aez")

    def test_l1empty(self):
        l2 = ["a", "z", "e"]
        assert not diffl([], l2)

    def test_both_empty(self):
        assert not diffl([], [])


class TestCheckMissingCols:
    def test_missing(self):
        inp = pd.DataFrame([[1, 2], [3, 4]], columns=["col0", "col1"])
        with pytest.raises(KeyError):
            check_missing_cols(inp, ["col0", "missing_cols"])

    def test_ok(self):
        inp = pd.DataFrame([[1, 2], [3, 4]], columns=["col0", "col1"])
        check_missing_cols(inp, ["col0", "col1"])


class TestMultiReplace:
    def test_standard(self):
        replaced = multi_replace("aaa bbb ccc", {"aaa": "ddd", "ccc": "eee"})
        assert replaced == "ddd bbb eee"

    def test_long_short_replace(self):
        replaced = multi_replace("aaa bbb ccc", {"aaa": "ddd", "aa": "ee"})
        assert replaced == "ddd bbb ccc"

    def test_no_matching(self):
        replaced = multi_replace("aaa bbb ccc", {"ddd": "eee"})
        assert replaced == "aaa bbb ccc"

    def test_case_sensitive(self):
        replaced = multi_replace("AAA bbb ccc", {"aaa": "eee", "AAA": "ddd"})
        assert replaced == "ddd bbb ccc"

    def test_empty_replacement(self):
        replaced = multi_replace("AAA bbb ccc", {})
        assert replaced == "AAA bbb ccc"

    def test_empty_input(self):
        replaced = multi_replace("", {"a": "b"})
        assert replaced == ""


class TestReadExternal:
    def test_list(self):
        d = {"b": ["a", "c"]}
        out = read_external(d, "b")
        assert out == ["a", "c"]

    def test_joblib(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            fn = os.path.join(tmpdirname, "test_joblib.joblib")
            clf = RandomForestClassifier()
            clf.features = ["a", "b", "c"]
            joblib.dump(clf, fn)
            d = {"a": 1, "b": fn}

            out = read_external(d, "b")
            assert out == ["a", "b", "c"]

    def test_dict(self):
        d = {"a": 1, "b": {"c": 2}}
        out = read_external(d, "b")
        assert out == {"c": 2}

    def test_json(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            fn = os.path.join(tmpdirname, "test_json.json")
            with open(fn, "w") as f:
                json.dump({"c": 2}, f)
            d = {"a": 1, "b": fn}

            out = read_external(d, "b")
            assert out == {"c": 2}

    def test_txt(self):
        with tempfile.TemporaryDirectory() as tmpdirname:
            fn = os.path.join(tmpdirname, "test_txt.txt")
            np.savetxt(fn, ["1", "b", "c"], fmt="%s")
            d = {"a": 1, "b": fn}

            out = read_external(d, "b")
            assert out == ["1", "b", "c"]
