from src.features.common import *

from . import *


class TestGeneralEncoder:
    def test_onehot(self):
        inp = pd.DataFrame({'col0': ['a', 'b', 'a'], 'col1': [1, 3, 2]})
        tf = GeneralEncoder(onehot=['col0'], onehot_opt=dict(dtype='int', sparse=False),
                            standard=['col1'], standard_opt={'with_std': 0},
                            passthrough=['col1'])
        out = tf.fit_transform(inp)
        inp = pd.DataFrame({'col0_a': [1., 0, 1], 'col0_b': [0., 1, 0], 'standard_col1': [-1., 1, 0],
                            'col1': [1., 3, 2]}).astype(np.float32)
        pd.testing.assert_frame_equal(out, inp)


class TestLogValues:
    def test_simple_2_columns(self):
        inp = pd.DataFrame([[0.1, 10, 7], [0.01, 100, 7]], columns=['col0', 'col1', 'not_used'])

        tf = Loggifier(['col0', 'col1'])

        out = tf.fit_transform(inp)

        np.testing.assert_almost_equal(out, np.array([[-1, 1], [-2, 2]]))

        out = tf.inverse_transform(out)
        np.testing.assert_almost_equal(out, inp[['col0', 'col1']].values)

    def test_error_raise(self):
        inp = pd.DataFrame([[-0.1]], columns=['col0'])
        with pytest.raises(ValueError):
            Loggifier(['col0']).fit_transform(inp)

    def test_zero_raise(self):
        inp = pd.DataFrame([[0]], columns=['col0'])
        with pytest.raises(ValueError):
            Loggifier(['col0'], zero_to='raise').fit_transform(inp)

    def test_error_negative_to_zero_raise(self):
        inp = pd.DataFrame([[-0.1]], columns=['col0'])
        with pytest.raises(ValueError):
            Loggifier(['col0'], zero_to='raise', errors='zero').fit_transform(inp)

    def test_0_to_min_no_min(self):
        inp = pd.DataFrame([[0]], columns=['col0'])
        tf = Loggifier(['col0'], zero_to='min')
        out = tf.fit_transform(inp)
        np.testing.assert_almost_equal(out, np.array([[0]]))

        out = tf.inverse_transform(out)
        np.testing.assert_almost_equal(out, np.array([[1]]))

    def test_0_to_min(self):
        inp = pd.DataFrame([[0, 0], [100, 0.01]], columns=['col0', 'col1'])
        tf = Loggifier(['col0', 'col1'], zero_to='min')
        out = tf.fit_transform(inp)
        np.testing.assert_almost_equal(out, np.array([[2, -2], [2, -2]]))

        out = tf.inverse_transform(out)
        np.testing.assert_almost_equal(out, [[100, 0.01], [100, 0.01]])

    def test_0_to_min1(self):
        inp = pd.DataFrame([[0, 0], [100, 0.01]], columns=['col0', 'col1'])
        tf = Loggifier(['col0', 'col1'], zero_to='min1')
        out = tf.fit_transform(inp)
        np.testing.assert_almost_equal(out, np.array([[1, -3], [2, -2]]))

        out = tf.inverse_transform(out)
        np.testing.assert_almost_equal(out, [[10, 0.001], [100, 0.01]])

    def test_0_to_0(self):
        inp = pd.DataFrame([[0]], columns=['col0'])
        tf = Loggifier(['col0'], zero_to='zero')
        out = tf.fit_transform(inp)

        np.testing.assert_almost_equal(out, np.array([[0]]))

        out = tf.inverse_transform(out)
        np.testing.assert_almost_equal(out, [[1]])

    def test_0_missing_values(self):
        inp = pd.DataFrame([[0.1, 10], [0.01, 100], [0, 0], [np.nan, np.nan]],
                           columns=['col0', 'col1'])
        tf = Loggifier(['col0', 'col1'], zero_to='min')
        out = tf.fit_transform(inp)

        np.testing.assert_almost_equal(out, np.array([[-1, 1], [-2, 2], [-2, 1], [np.nan, np.nan]]))

        out = tf.inverse_transform(out)
        np.testing.assert_almost_equal(out, [[0.1, 10], [0.01, 100], [0.01, 10], [np.nan, np.nan]])

    def test_negative_to_zero_to_zero(self):
        inp = pd.DataFrame([[-10]], columns=['col0'])
        tf = Loggifier(['col0'], zero_to='zero', errors='zero')
        out = tf.fit_transform(inp)

        np.testing.assert_almost_equal(out, np.array([[0]]))

        out = tf.inverse_transform(out)
        np.testing.assert_almost_equal(out, [[1]])

    def test_negative_to_abs(self):
        inp = pd.DataFrame([[-10]], columns=['col0'])
        tf = Loggifier(['col0'], errors='abs')
        out = tf.fit_transform(inp)

        np.testing.assert_almost_equal(out, np.array([[1]]))

        out = tf.inverse_transform(out)
        np.testing.assert_almost_equal(out, [[10]])

    def test_negative_to_sign(self):
        inp = pd.DataFrame({'col0': [-10, 0, 0.3, -0.2]})
        tf = Loggifier(['col0'], errors='sign')
        out = tf.fit_transform(inp)
        np.testing.assert_almost_equal(out, np.array([[-1, 0, 0, 0]]).T)

        out = tf.inverse_transform(out)
        np.testing.assert_almost_equal(out, np.array([-10, 0, 0, 0]).reshape(-1, 1))


class TestLimitPrecision:
    def test_larger1(self):
        inp = pd.DataFrame([[1.3456]], columns=['a'])

        tf = PrecisionLimiter(columns=['a'], precision=3)
        out = tf.fit_transform(inp)

        np.testing.assert_array_equal(out, [[1.34]])

    def test_smaller1(self):
        inp = pd.DataFrame([[0.00123456]], columns=['a'])

        tf = PrecisionLimiter(columns=['a'], precision=3)
        out = tf.fit_transform(inp)

        np.testing.assert_almost_equal(out, [[0.00123]])

    def test_special(self):
        inp = pd.DataFrame([[0.], [1.]], columns=['a'])

        tf = PrecisionLimiter(columns=['a'], precision=3)
        out = tf.fit_transform(inp)

        np.testing.assert_array_equal(out, [[0.], [1.]])

    def test_negative(self):
        inp = pd.DataFrame([[-1.234]], columns=['a'])

        tf = PrecisionLimiter(columns=['a'], precision=3)
        out = tf.fit_transform(inp)

        np.testing.assert_array_equal(out, [[-1.24]])


class TestMapper:
    def setup(self):
        self.indic = Mapper(mapper={'key': 'value'}, main_col='main_col')

    def test_ok(self):
        assert self.indic.transform({'main_col': 'key'}) == 'value'

    def test_transform_value_absent_from_mapper_df(self):
        out = self.indic.transform(pd.Series(['unknown_key']))
        assert out.shape == (1,)
        assert out.isnull().sum() == 1

    def test_transform_main_col_absent_from_data_dict(self):
        out = self.indic.transform({'other_col': 'unknown_key'})
        assert out is None

    def test_transform_value_absent_from_mapper_dict(self):
        out = self.indic.transform({'main_col': 'unknown_key'})
        assert out is None

    def test_transform_ok_serie(self):
        out = self.indic.transform(pd.Series(['key']))
        assert out.shape == (1,)
        assert out.iloc[0] == 'value'

    def test_transform_ok_dict(self):
        out = self.indic.transform({'main_col': 'key'})
        assert out == 'value'


class TestFrameMapper:
    def test_frame_transform(self):
        inp = pd.DataFrame({'a': [3, 2, 3, 1], 'b': [4, 5, 6, 7]})
        mapper = FrameMapper(pd.DataFrame({'first': [1, 2, 3], 'second': [4, 5, 6]}, index=[1, 2, 3]),
                             main_cols='a')
        out = mapper.transform(inp)
        np.testing.assert_array_equal(out, [[3, 6], [2, 5], [3, 6], [1, 4]])

    def test_frame_transform_dict(self):
        inp = {'a': 3, 'b': 4}
        mapper = FrameMapper(pd.DataFrame({'first': [1, 2, 3], 'second': [4, 5, 6]}, index=[1, 2, 3]),
                             main_cols='a')
        out = mapper.transform(inp)
        assert out == {'first': 3, 'second': 6}

    def test_frame_transform_dict_no_match(self):
        inp = {'a': 7, 'b': 4}
        mapper = FrameMapper(pd.DataFrame({'first': [1, 2, 3], 'second': [4, 5, 6]}, index=[1, 2, 3]),
                             main_cols='a')
        assert not mapper.transform(inp)

    def test_mapper_2Dcolumns_frame(self):
        inp = pd.DataFrame({'a': [3, 2, 3, 1], 'b': [4, 5, 4, 7]})
        mapper = pd.DataFrame({'first': [1, 2, 3], 'second': [4, 5, 6],
                               'c': [1, 2, 3], 'd': [7, 5, 4]}).set_index(['c', 'd'])
        mapper = FrameMapper(mapper, main_cols=['a', 'b'])
        out = mapper.transform(inp)
        np.testing.assert_array_equal(out, [[3, 6], [2, 5], [3, 6], [1, 4]])


class TestCSVMapper:
    def setup_class(self):
        self.fn = os.path.join(os.path.dirname(__file__), 'data', 'test_mapper.csv')

    def test_missing_value(self):
        out = CSVMapper(self.fn).transform({'main_col': 'unknown_key'})
        assert out is None

    def test_ok(self):
        out = CSVMapper(self.fn).transform({'main_col': 'val0'})
        assert out == 1

    def test_unknown_file(self):
        with pytest.raises(IOError):
            CSVMapper('unknown_file')


class TestCSVMapperMapperFromCSV:
    def setup_class(self):
        self.fn = os.path.join(os.path.dirname(__file__), 'data', 'test_mapper.csv')

    def test_unknown_file(self):
        with pytest.raises(IOError):
            CSVMapper.mapper_from_csv('unknown_file')

    def test_no_value_column(self):
        fn = os.path.join(os.path.dirname(__file__), 'data', 'test_mapper_no_value.csv')
        with pytest.raises(KeyError):
            CSVMapper.mapper_from_csv(fn)

    def test_error_no_main_col_column(self):
        fn = os.path.join(os.path.dirname(__file__), 'data', 'test_mapper_no_main_col.csv')
        with pytest.raises(KeyError):
            CSVMapper.mapper_from_csv(fn)

    def test_error_extra_params(self):
        with pytest.raises(TypeError):
            CSVMapper.mapper_from_csv(self.fn, unknown_param=4)

    def test_ok(self):
        mapper = CSVMapper.mapper_from_csv(self.fn)
        assert mapper == ({'val0': 1, 'val1': 2}, 'main_col')


class TestJSONMapper:
    def setup_class(self):
        self.fn = os.path.join(os.path.dirname(__file__), 'data', 'test_mapper.json')
        self.indic = JSONMapper(fn=self.fn, main_col='main_col')

    def test_missing_value(self):
        out = self.indic.transform({'main_col': 'unknown_key'})
        assert out is None

    def test_ok(self):
        out = self.indic.transform({'main_col': 'key0'})
        assert out == 'value0'

    def test_unknown_file(self):
        with pytest.raises(IOError):
            JSONMapper('unknown_file', 'main_col')


class TestJSONMapperMapperFromJSON:
    def setup_class(self):
        self.fn = os.path.join(os.path.dirname(__file__), 'data', 'test_mapper.json')

    def test_unknown_file(self):
        with pytest.raises(IOError):
            JSONMapper.mapper_from_json('unknown_file')

    def test_ok(self):
        mapper = JSONMapper.mapper_from_json(self.fn)
        assert mapper == {'key0': 'value0'}


class TestFilterNumentries:
    def test_2_should_disapear(self):
        inp = pd.DataFrame({'group': [1, 1, 1, 2, 2],
                            'target': [1, 2, 3, 4, 5]})
        out = filter_num_entries(inp, main_cols=['group'], num_entries=3)

        exp = inp[inp.group == 1]
        pd.testing.assert_frame_equal(out, exp)


class TestAggSerieValue:
    def test_median_group_no_sum(self):
        inp = pd.DataFrame({'group': [1, 2, 3, 1, 2, 3],
                            'date': pd.date_range('2020-01-01', '2020-01-06', freq='1d'),
                            'target': [1, 2, 3, 4, 5, 6]})

        tf = AggSerieValue(main_cols=['group'], target='target')
        out = tf.fit_transform(inp)
        exp = np.array([2.5, 3.5, 4.5]*2).reshape(-1, 1)
        np.testing.assert_array_equal(out, exp)

    def test_mean_group_with_sum(self):
        inp = pd.DataFrame({'group': [1, 2, 1, 1, 2, 1],
                            'date': list(pd.date_range('2020-01-01', '2020-01-03', freq='1d'))*2,
                            'target': [1, 2, 3, 4, 5, 6]})

        tf = AggSerieValue(main_cols=['group'], target='target', target_fct=np.sum, group_fct=np.mean)
        out = tf.fit_transform(inp)

        exp = np.array([7, 3.5, 7, 7, 3.5, 7]).reshape(-1, 1)
        np.testing.assert_array_equal(out, exp)


class TestDateCorrelationEmbedding:
    def test_simple(self):
        inp = pd.DataFrame({'group': [1, 1, 1, 2, 2, 2, 3, 3, 3],
                            'date': list(pd.date_range('2020-01-01', '2020-01-03', freq='1d'))*3,
                            'target': [1, 2, 3, 5, 4, 6, 7, 9, 8]})

        tf = DateCorrelationEmbedding(main_cols=['group'], target='target', num_entries=1,
                                      umap_opts={'n_neighbors': 2, 'n_components': 1})
        out = tf.fit_transform(inp)

        exp = np.array([[-30.263084]]*3 + [[-32.417503]]*3 + [[-30.962118]]*3)
        np.testing.assert_almost_equal(out, exp, 6)
