from src.data.walmart import *

from . import *


class TestFillNAClosest:
    def test_standard(self):
        inp = pd.DataFrame({"emb": np.arange(5),
                            'value': [1, 2, 3, np.nan, 5]})

        tf = FillNAClosest(n_neighbors=2, emb_cols=['emb'], targets=['value'])
        out = tf.fit_transform(inp)

        exp = inp.fillna(4)
        pd.testing.assert_frame_equal(out, exp)

    def test_with_duplicates(self):
        inp = pd.DataFrame({"emb": [0, 1, 1.5, 3, 4, 4, 4],
                            'value': [1, 2, 3, np.nan, 5, 5, 5]})

        tf = FillNAClosest(n_neighbors=2, emb_cols=['emb'], targets=['value'])
        out = tf.fit_transform(inp)

        exp = inp.fillna(4)
        pd.testing.assert_frame_equal(out, exp)


class TestUndiffPerGroup:
    def test_standard(self):
        inp = pd.DataFrame({'store': [1, 1, 1, 2, 2, 2],
                            'dept': [1, 1, 1, 1, 1, 1],
                            'date': ['2020-01-01', '2020-02-01', '2020-03-01']*2,
                            'median_weekly_sales_store_dept': [10, 10, 10, 1000, 1000, 1000],
                            'last_week_weekly_sales': [20, 10, 30, 900, 800, 1000],
                            'weekly_sales': [10, 30, 5, 800, 1000, 1500]})
        inp['preds'] = (inp['weekly_sales']-inp['last_week_weekly_sales'])/inp['median_weekly_sales_store_dept']
        inp['date'] = pd.to_datetime(inp['date'])

        out = undiff_per_group(inp)
        np.testing.assert_array_equal(out['preds_sales'], out['weekly_sales'])
