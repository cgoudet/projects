import pytest

from src.app import DB, create_app


@pytest.fixture(scope="session")
def test_app():
    return create_app("TEST")


@pytest.fixture
def client(test_app):
    return test_app.test_client()


@pytest.fixture
def db(test_app):
    with test_app.app_context():
        DB.create_all()
        yield DB
        DB.drop_all()
        DB.session.commit()


pytest_plugins = [
    "test.data.starcraft.fixtures.fixtures",
]
