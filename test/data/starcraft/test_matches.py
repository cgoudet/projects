from datetime import datetime

from src.data.starcraft.matches import save_match_api_output, update_map_pool
from src.data.starcraft.models import Map, Match


def test_update_map_pool_add_new_map(db):
    raw_matches = {"matches": [{"map": "test_map"}]}
    update_map_pool(raw_matches)

    assert Map.query.count() == 1


def test_update_map_pool_do_not_add_duplicate_in_same_file(db):
    raw_matches = {"matches": [{"map": "test_map"}, {"map": "test_map"}]}
    update_map_pool(raw_matches)

    assert Map.query.count() == 1


def test_update_map_pool_add_only_unknown_maps(db):
    raw_matches = {"matches": [{"map": "test_map"}]}
    update_map_pool(raw_matches)

    assert Map.query.count() == 1

    raw_matches = {"matches": [{"map": "test_map"}, {"map": "second_map"}]}
    update_map_pool(raw_matches)

    assert Map.query.count() == 2
    assert Map.query.get(2).name == "second_map"


def test_save_match_api_output(db, raw_matches, players):
    assert Match.query.count() == 0
    assert Map.query.count() == 0

    save_match_api_output(raw_matches, players[0])

    assert Match.query.count() == 2
    assert Map.query.count() == 2

    match = Match.query.get(1)
    assert match.player_id == 9635739
    assert match.decision == 5
    assert match.dt == datetime(2021, 4, 3, 20, 9, 35)
