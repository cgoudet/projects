import json
from pathlib import Path

import pytest
from src.data.starcraft.models import Ladder, Map, Match, Player


@pytest.fixture
def match_maps(db):
    test_maps = [
        "Jagannatha LE",
        "Lightshade LE",
        "Romanticide LE",
        "Oxide LE",
        "Deathaura LE",
        "Pillars of Gold LE",
    ]
    maps = [Map(name=name) for name in test_maps]
    [db.session.add(m) for m in maps]
    db.session.commit()
    return maps


@pytest.fixture
def players(ladder):
    fn = Path(__file__).parent / "raw_players.json"
    with fn.open() as f:
        raw_players = json.load(f)

    players = Player.players_from_api_output(raw_players, ladder)
    return players


@pytest.fixture
def raw_matches():
    fn = Path(__file__).parent / "raw_matches.json"
    with fn.open() as f:
        raw_matches = json.load(f)
    return raw_matches


@pytest.fixture
def matches(players, raw_matches, db, match_maps):
    fn = Path(__file__).parent / "raw_matches.json"
    with fn.open() as f:
        raw_matches = json.load(f)

    matches = Match.matches_from_api_output(raw_matches, players[0])
    return matches


@pytest.fixture
def ladder():
    fn = Path(__file__).parent / "raw_ladder.json"
    with fn.open() as f:
        raw_ladder = json.load(f)

    ladder = Ladder.from_api_output(raw_ladder, "eu")
    return ladder
