from datetime import datetime, timedelta

import numpy as np
from src.app.models import save_multiple
from src.data.starcraft.models import Ladder, Map, Match, Player


def test_ladder_from_api_output(ladder):

    exp = Ladder(id=234881, region="eu", league=6, season=46, queue=201, team_type=0)
    for attr in ["id", "region", "league", "season", "queue", "team_type"]:
        assert getattr(ladder, attr) == getattr(exp, attr)


def test_players_from_api_output(players, ladder, db):

    assert len(players) == 2

    exp = Player(
        blizzard_id=9635739,
        name="无奈错过棒棒糖",
        race="P",
        profile="/profile/5/1/9635739",
        ladder_id=ladder.id,
    )

    for attr in ["blizzard_id", "name", "race", "profile", "ladder_id"]:
        assert getattr(players[0], attr) == getattr(exp, attr)
    assert players[0].clan is None
    assert players[0].dt is None


def test_barcode_player_has_empty_name():
    raw_player = {
        "character": {
            "id": "9635739",
            "realm": 1,
            "region": 5,
            "displayName": "IIIlllll",
            "clanName": "",
            "clanTag": "",
            "profilePath": "/profile/5/1/9635739",
        },
        "joinTimestamp": 1613995171,
        "points": 2851,
        "wins": 107,
        "losses": 57,
        "highestRank": 200,
        "previousRank": 1,
        "favoriteRaceP1": "PROTOSS",
    }
    player = Player.from_api_output(raw_player)

    assert not player.name


def test_players_defaut_dt_value(db, ladder, players):
    save_multiple(players)
    assert Player.query.count() == 2

    player = Player.query.get(1)
    assert np.abs(player.dt - datetime.utcnow()).seconds < 1

    exp = Player(
        blizzard_id=9635739,
        name="无奈错过棒棒糖",
        race="P",
        profile="/profile/5/1/9635739",
        ladder_id=ladder.id,
    )

    for attr in ["blizzard_id", "name", "race", "profile", "ladder_id"]:
        assert getattr(player, attr) == getattr(exp, attr)
    assert player.clan is None


def test_equal_players():
    first = Player(
        id=2,
        blizzard_id=9635739,
        name="无奈错过棒棒糖",
        race="P",
        profile="/profile/5/1/9635739",
        ladder_id=1,
        dt=datetime.utcnow(),
    )
    second = Player(
        id=3,
        blizzard_id=9635739,
        name="无奈错过棒棒糖",
        race="P",
        profile="/profile/5/1/9635739",
        ladder_id=1,
        dt=datetime.utcnow() - timedelta(hours=1),
    )
    assert first == second


def test_different_players():
    first = Player(
        id=2,
        blizzard_id=9635739,
        name="无奈错过棒棒糖",
        race="P",
        profile="/profile/5/1/9635739",
        ladder_id=5,
        dt=datetime.utcnow(),
    )
    second = Player(
        id=2,
        blizzard_id=23,
        name="无奈错过棒棒糖",
        race="P",
        profile="/profile/5/1/9635739",
        ladder_id=5,
        dt=first.dt,
    )
    assert first != second


def test_matches_from_api_output(matches, db):

    assert len(matches) == 2

    exp = Match(
        id=None,
        map_id=1,
        type_id=1,
        player_id=9635739,
        decision=5,
        dt=datetime(2021, 4, 3, 20, 9, 35),
    )

    for attr in ["id", "map_id", "type_id", "player_id", "decision", "dt"]:
        assert getattr(matches[0], attr) == getattr(exp, attr)


def test_matches_from_empty_api_output(db, players):
    matches = Match.matches_from_api_output([], players[0])
    assert not matches


def test_save_matches(matches, db):
    save_multiple(matches)
    assert Match.query.count() == 2

    exp = Match(
        id=1,
        map_id=1,
        type_id=1,
        player_id=9635739,
        decision=5,
        dt=datetime(2021, 4, 3, 20, 9, 35),
    )

    match = Match.query.get(1)
    for attr in ["id", "map_id", "type_id", "player_id", "decision", "dt"]:
        assert getattr(match, attr) == getattr(exp, attr)


def test_add_duplicate_matches(matches, db):
    save_multiple(matches)
    assert Match.query.count() == 2

    matches[1].dt = datetime(2021, 4, 4, 20, 9, 35)
    save_multiple(matches)

    assert Match.query.count() == 3


def test_map_from_api_output(raw_matches, db):
    content = raw_matches["matches"][0]
    m = Map.from_matches_api_output(content)
    save_multiple([m])
    assert Map.query.count() == 1


def test_save_map_duplicate(raw_matches, db):
    content = raw_matches["matches"][0]
    m = Map.from_matches_api_output(content)
    save_multiple([m, m])
    assert Map.query.count() == 1
