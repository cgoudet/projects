from src.app.models import save_multiple
from src.data.starcraft.models import Player
from src.data.starcraft.player import save_player_snapshot


def test_save_players_with_same_before_last_info(db, ladder, players):
    player = players[0]
    save_multiple([player])

    player.race = "T"
    save_multiple([player])
    assert Player.query.count() == 2

    player.race = "P"
    save_player_snapshot(player)
    assert Player.query.count() == 3


def test_save_players_with_same_last_info_do_nothing(db, players):
    player = players[0]
    save_multiple([player])
    assert Player.query.count() == 1

    save_player_snapshot(player)
    assert Player.query.count() == 1


def test_save_players_with_no_previous_snapshot(db, players):
    assert Player.query.count() == 0

    player = players[0]
    save_player_snapshot(player)
    assert Player.query.count() == 1
