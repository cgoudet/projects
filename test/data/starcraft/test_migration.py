import json
import os
from pathlib import Path

import numpy as np
import pandas as pd
import pytest
from src.data.starcraft.pipeline import LadderAPI, MapAPI, MatchAPI, PlayerAPI
from src.utils.utils import diffl

TESTDBNAME = Path("/tmp/tu_starcraft.db")
TESTDIR = Path(__file__).parent / ".."


class TestPlayerAPI:
    def setup_method(self):
        self.player = PlayerAPI(hed=1, dbname=TESTDBNAME)

    def teardown_method(self):
        os.remove(TESTDBNAME)

    def test_format_players(self):
        with (TESTDIR / "test_data_starcraft_ladder_players.json").open() as f:
            players = json.load(f)

        out = self.player.format_players(players)
        assert len(out) == 3

        simple = {
            "clan": "Ence",
            "name": "Serral",
            "player_id": 315071,
            "profile": "/profile/2/1/315071",
            "race": "Z",
        }
        assert out[0] == simple

        assert out[1]["name"] == ""

        assert out[2]["race"] == "RT"

    def test_format_players_errors_diff_profil(self):
        with (TESTDIR / "test_data_starcraft_ladder_players.json").open() as f:
            players = json.load(f)
            players["ladderMembers"][-1]["character"]["displayName"] = "different_name"
        with pytest.raises(AssertionError):
            self.player.format_players(players)

    def test_insert_player_with_date(self):
        players = pd.DataFrame(
            [
                {
                    "name": "Serral",
                    "ladder_id": 1,
                    "dt": 5,
                    "race": "R",
                    "profile": "profile",
                    "player_id": 1,
                    "clan": "",
                }
            ]
        )
        self.player.create_table()
        self.player.insert(players)

        out = pd.read_sql("select * from players;", self.player.conn)
        cols = sorted(players.columns)
        pd.testing.assert_frame_equal(out[cols], players[cols])

    def test_player_last_status(self):
        players = pd.DataFrame(
            [
                {
                    "name": "Serral",
                    "ladder_id": 1,
                    "profile": "profile",
                    "player_id": 1,
                    "clan": "",
                }
            ]
            * 2
        )
        players["dt"] = [1, 2]
        players["race"] = ["Z", "R"]
        self.player.create_table()
        self.player.insert(players)

        out = self.player.players_last_status()
        cols = diffl(players.columns, ["dt"])
        np.testing.assert_array_equal(
            out.iloc[:1][cols].values, players.iloc[1:][cols].values
        )

    def test_save(self):
        players = pd.DataFrame(
            [
                {
                    "name": "Serral",
                    "ladder_id": 1,
                    "race": "Z",
                    "profile": "profile",
                    "player_id": 1,
                    "clan": "",
                },
                {
                    "name": "Harstem",
                    "ladder_id": 2,
                    "race": "P",
                    "profile": "profile",
                    "player_id": 2,
                    "clan": "clan",
                },
            ]
        )
        players["dt"] = [1, 2]
        self.player.create_table()
        self.player.insert(players)

        players = [
            {
                "name": "Serral",
                "ladder_id": 1,
                "race": "Z",
                "profile": "profile",
                "player_id": 1,
                "clan": "",
            },
            {
                "name": "Harstem",
                "ladder_id": 2,
                "race": "PTZ",
                "profile": "profile",
                "player_id": 2,
                "clan": "clan",
            },
            {
                "name": "Reynor",
                "ladder_id": 1,
                "race": "Z",
                "profile": "profile",
                "player_id": 3,
                "clan": "clan",
            },
        ]

        self.player.save(players)

        out = pd.read_sql("select * from players;", self.player.conn).sort_values(
            ["player_id", "race"]
        )
        assert list(out["name"]) == ["Serral", "Harstem", "Harstem", "Reynor"]
        assert list(out["race"]) == ["Z", "P", "PTZ", "Z"]
        assert (out["dt"] > 3).mean() == 0.5


class TestMapAPI:
    def setup_method(self):
        self.mapapi = MapAPI(dbname=TESTDBNAME)

    def teardown_method(self):
        os.remove(TESTDBNAME)

    def test_create_db(self):
        self.mapapi.create_table()
        self.mapapi.update_map_base(["first", "second"])

        out = pd.read_sql("select * from maps", self.mapapi.conn).sort_values("id")
        np.testing.assert_array_equal(
            out[["id", "name"]].values,
            np.array([[1, "first"], [2, "second"]], dtype="object"),
        )
