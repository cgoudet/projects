from src.models.metrics import *

from . import *


class TestExpError:
    def test_absolute(self):
        truth = np.array([20, 9]).reshape(-1, 1)
        preds = np.array([10, 10]).reshape(-1, 1)

        log = Loggifier(errors='sign')
        truth = log.transform(truth)
        preds = log.transform(preds)

        sample_weight = np.array([2, 1])
        out = exp_error(truth, preds, sample_weight)
        assert pytest.approx(out, 1e-5) == 7.

    def test_other_base(self):
        truth = np.array([20, 9]).reshape(-1, 1)
        preds = np.array([10, 10]).reshape(-1, 1)

        log = Loggifier(errors='sign')
        truth = log.transform(truth)
        preds = log.transform(preds)

        sample_weight = np.array([2, 1])
        out = exp_error(truth, preds, sample_weight, base=mean_squared_error)
        assert pytest.approx(out, 1e-5) == 201/3
