from src import *
from src.models.common import *


class TestLastYear:
    def test_standard(self):
        X = np.array([[1, 1, 1, 2],  # group
                      [1, 1, 2, 1],  # time feature
                      ]).T
        y = np.array([0, 1, 2, 3])

        reg = LastYear(groups=[0], step_col=1, fillna=-1)

        reg.fit(X, y)

        out = reg.predict(X)
        np.testing.assert_array_equal(out, [-1, -1, 1, -1])
