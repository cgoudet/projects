import os
from copy import copy

from src import ROOTDIR

DATADIR = ROOTDIR / "data" / "raw"
SUBPROJECTS = ["presse", "starcraft"]

PORTS = {"presse": 5433, "starcraft": 5434}

POSTGRES = {
    "user": "postgres",
    "pw": "postgres",
    "host": "localhost",
}


class Config(object):
    """
    Common configurations
    """

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROJECTS = ["presse"]

    @property
    def SQLALCHEMY_DATABASE_URI(self):
        return "sqlite:///" + str(DATADIR / f"app_{self.MODE.lower()}.db")

    @property
    def MIGRATION_DIRECTORY(self):
        return str(ROOTDIR / "data" / "migrations" / f"migration_{self.MODE.lower()}")

    @property
    def SQLALCHEMY_BINDS(self):
        return {
            k: "sqlite:///" + str(DATADIR / f"{k}/{k}_{self.MODE.lower()}.db")
            for k in SUBPROJECTS
        }


class DevelopmentConfig(Config):
    """
    Development configurations
    """

    MODE = "DEV"
    DEBUG = False
    # SQLALCHEMY_ECHO = True


class ProductionConfig(Config):
    """
    Production configurations
    """

    DEBUG = False
    MODE = "PROD"

    @property
    def SQLALCHEMY_BINDS(self):
        db_params = "postgresql://%(user)s:%(pw)s@%(host)s:{}/{}" % POSTGRES
        return {k: db_params.format(PORTS[k], f"{k}_prod") for k in SUBPROJECTS}


class TestConfig(Config):
    DEBUG = True
    SQLALCHEMY_ECHO = False
    MODE = "TEST"

    @property
    def SQLALCHEMY_DATABASE_URI(self):
        return "sqlite:////tmp/test_flask.db"


app_config = {"DEV": DevelopmentConfig, "PROD": ProductionConfig, "TEST": TestConfig}
