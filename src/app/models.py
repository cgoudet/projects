from sqlalchemy import event
from sqlalchemy.engine import Engine
from sqlalchemy.sql import Insert

from . import DB

_ignore_tables = set()

from sqlalchemy.dialects.postgresql.dml import OnConflictDoNothing
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.sql.expression import Insert


@compiles(Insert, "postgresql")
def prefix_inserts(insert, compiler, **kw):
    insert._post_values_clause = OnConflictDoNothing()
    return compiler.visit_insert(insert, **kw)


@compiles(Insert, "sqlite")
def prefix_inserts(insert, compiler, **kw):
    return compiler.visit_insert(
        insert.prefix_with("OR IGNORE", dialect="sqlite"), **kw
    )


def save_multiple(elements):
    DB.session.bulk_save_objects(elements)
    DB.session.commit()


if True:
    from src.data.presse import models
    from src.data.starcraft import models
