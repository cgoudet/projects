from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from .config import app_config

DB = SQLAlchemy()
MIGRATE = Migrate()


def create_app(config_name="DEV"):
    app = Flask(__name__, instance_relative_config=True)
    conf = app_config[config_name]()
    app.config.from_object(conf)

    DB.init_app(app)
    MIGRATE.init_app(app, DB, directory=conf.MIGRATION_DIRECTORY)

    return app


from . import models
