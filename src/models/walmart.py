from sklearn.compose import TransformedTargetRegressor
from sklearn.linear_model import ElasticNet, LinearRegression
from sklearn.metrics import mean_absolute_error
from statsmodels.tsa.holtwinters import ExponentialSmoothing

from ..data.walmart import undiff_per_group
from ..features.common import *
from . import *
from .common import *


class WalmartPredictor(BaseEstimator, RegressorMixin):
    pipe_ = None

    def __init__(self, relative=False):
        self.relative = relative
        self.target_ = 'weekly_sales'
        if relative:
            self.target_ = 'relative_' + self.target_

    def fit(self, df, y=None, **kwargs):
        print(df)
        print(df.columns)
        assert self.pipe_ is not None
        y = df[self.target_]
        self.pipe_.fit(df.drop(columns=self.target_), y, **kwargs)
        return self

    def predict(self, X):
        assert self.pipe_ is not None
        out = self.pipe_.predict(X.drop(columns=self.target_, errors='ignore'))
        if self.relative:
            out = out * X['median_weekly_sales_store']
        return out

    def score(self, X):
        assert self.pipe_ is not None
        preds = self.predict(X)
        score = mean_absolute_error(X.weekly_sales, preds, sample_weight=1+X.is_holiday*4)
        return score

    def submission(self, X, savename=None):
        id = (X.store.astype(str) + '_'
              + X.dept.astype(str) + '_'
              + X.date.dt.strftime('%Y-%m-%d'))
        preds = self.predict(X)
        res = pd.DataFrame({'Id': id, 'Weekly_Sales': preds})

        if savename:
            savename = ROOTDIR / 'data' / 'processed' / 'walmart' / f'{savename}.csv'
            res.to_csv(savename, index=False)
            joblib.dump(self, savename.with_suffix('.joblib'))

        return res


class LogitDept(WalmartPredictor):
    def __init__(self):

        ct = GeneralEncoder(onehot=['store', 'dept'], onehot_opt=dict(sparse=False))

        reg = TransformedTargetRegressor(LinearRegression(),
                                         transformer=Loggifier(errors='sign'))
        reg = LinearRegression()
        self.pipe_ = make_pipeline(ct, reg)


def submission(df, savename=None):
    id = (df.store.astype(str) + '_'
          + df.dept.astype(str) + '_'
          + df.date.dt.strftime('%Y-%m-%d'))
    res = pd.DataFrame({'Id': id, 'Weekly_Sales': df['preds']})

    if savename:
        savename = ROOTDIR / 'data' / 'processed' / 'walmart' / f'{savename}.csv'
        res.to_csv(savename, index=False)

    return res


class LGBRegressor(BaseEstimator, RegressorMixin):
    def __init__(self, clf_opt, fit_params, features):
        self.base_ = lgb.LGBMRegressor(**clf_opt)
        self.fit_params = fit_params
        self.features = features

    def fit(self, X, y=None):
        self.base_.fit(X[self.features], y, **self.fit_params)
        return self.base_

    def predict(self, X):
        return self.base_.predict(X[self.features])


def gbdt_standard(train, valid):
    clf_opt = dict(boosting_type='goss',
                   colsample_bytree=1,
                   min_child_weight=0.1,
                   num_leaves=10,
                   objective='mae',
                   reg_alpha=0,
                   reg_lambda=0,
                   n_estimators=1000,
                   learning_rate=0.05,
                   max_bin=1000,
                   n_jobs=30,
                   subsample=1
                   )
    categories = ['dept', 'store', 'week']
    features = list(np.loadtxt(ROOTDIR / 'references' / 'walmart' / 'basic_columns.txt', dtype=str))
    fit_params = {
        "early_stopping_rounds": 30,
        "eval_metric": "l1",
        "verbose": 0,
        "categorical_feature": [features.index(c) for c in categories],
        'eval_set': [(valid[features], valid.weekly_sales)],
        'eval_names': 'valid'
    }
    model = LGBRegressor(clf_opt, fit_params, features=features)
    return model


def rf_standard(train, valid):
    features = list(np.loadtxt(ROOTDIR / 'references' / 'walmart' / 'basic_columns.txt', dtype=str))

    clf_opt = {"n_estimators": 500,
               "min_weight_fraction_leaf": 0.00001,
               "min_impurity_decrease": 0.1,
               "n_jobs": 30}
    model = RandomForestRegressor(**clf_opt)
    model.features = features
    return model


def metrics(reg, train, valid, name):
    train = train.dropna(subset=reg.features)
    logger = get_logger(ROOTDIR / 'data' / 'interim' / 'walmart.log')
    start = time.time()
    reg.fit(train[reg.features], train.weekly_sales)
    end = time.time()

    metrics = {
        'fct': name,
        'duration': end-start,
        'train_score': mean_absolute_error(train.weekly_sales, reg.predict(train[reg.features])),
        'valid_score': mean_absolute_error(valid.weekly_sales, reg.predict(valid[reg.features]))
    }
    logger.info(msg='metrics', extra=metrics)
    print(metrics)


def last_year_linear(train, valid):
    model = LinearRegression()
    model.features = ['is_holiday', 'last_year_weekly_sales', 'median_weekly_sales_dept']
    return model


def rf_loggifier(train, valid):
    features = list(np.loadtxt(ROOTDIR / 'references' / 'walmart' / 'basic_columns.txt', dtype=str))

    clf_opt = {"n_estimators": 500,
               "min_weight_fraction_leaf": 0.002,
               "n_jobs": 30}
    base = RandomForestRegressor(**clf_opt)
    model = LogRegressor(base, errors='zero', zero_to='zero')
    model.features = features
    return model


def last_year(train, valid):
    model = LastYear([0, 1, 2], step_col=3)
    model.features = ['store', 'dept', 'week', 'year']
    return model


class RelativeYear(BaseEstimator, RegressorMixin):
    def __init__(self, base):
        self.base = base
        self.target = 'diff_year_relative_weekly_sales'
        self.train_feat = list(np.loadtxt(ROOTDIR / 'references' / 'walmart' / 'basic_columns.txt', dtype=str))
        self.features = self.train_feat + [self.target, 'date', 'last_year_weekly_sales']

        self.__name__ = 'relative_year_' + self.base.__class__.__name__

    def fit(self, X, y=None, sample_weight=None):
        y = X[self.target]
        self.base.fit(X[self.train_feat], y)

    def predict(self, X):
        tmp = X.copy()
        tmp['preds'] = self.base.predict(X[self.train_feat])
        tmp['preds'] *= tmp['median_weekly_sales_store_dept']
        tmp['preds'] += tmp['last_year_weekly_sales']
        return tmp['preds']


class GroupHolt(BaseEstimator, RegressorMixin):
    features = ['date', 'weekly_sales', 'store', 'dept']
    __name__ = "GroupHolt"
    def fit(self, X, y=None):
        self.models = {}
        start = X[X.weekly_sales != 0].groupby(['store', 'dept']).agg({'date': np.min}).reset_index()
        for i, (store, dept, date) in enumerate(start.values):
            print(f'{i+1}/{len(start)} {store} {dept}')
            mask = (X.store == store) & (X.dept == dept) & (X.date >= date)
            serie = pd.Series(y[mask].values, index=pd.DatetimeIndex(X.loc[mask, 'date'], freq='W-FRI'))
            if len(serie) < 52:
                continue
            model = ExponentialSmoothing(serie, trend='add', seasonal='add', seasonal_periods=52,
                                         initial_seasonal=serie.iloc[:52],
                                         damped_trend=True,
                                         initialization_method='known',
                                         initial_level=0,
                                         initial_trend=0)
            model.fit()
            self.models[(store, dept)] = model
        return self

    def predict(self, X):
        g = X.groupby(['store', 'dept']).agg({'date': [np.min, np.max]})
        g.columns = ['min_date', 'max_date']
        g = g.reset_index()
        res = []
        for _, row in g.iterrows():
            model = self.models[(row['store'], row['dept'])]
            rg = pd.date_range(row['min_date'], row['max_date'], freq='7D', closed=None)
            out = model.predict(model.params, rg[0], rg[-1])
            out = pd.DataFrame({'date': rg, 'preds_sales': out,
                                'dept': row['dept'],
                                'store': row['store']})
            res.append(out)

        res = pd.concat(res, axis=0, ignore_index=True)
        res = pd.merge(X, res, on=['store', 'dept', 'date'])
        return res.preds_sales.values


def train_models():
    DATADIR = ROOTDIR / 'data' / 'interim' / 'walmart'
    train = pd.read_parquet(DATADIR / 'walmart_train.parquet')
    train = train[train.dept == 1]
    valid = pd.read_parquet(DATADIR / 'walmart_valid.parquet')
    valid = valid[valid.dept == 1]

    fcts = [
        # gbdt_standard,
        # rf_standard
        # last_year_linear,
        # rf_loggifier,
        # last_year,
        # RelativeYear(RandomForestRegressor(n_estimators=500, n_jobs=30, min_weight_fraction_leaf=2e-3)),
        GroupHolt(),
    ]
    for fct in fcts:
        reg = fct(train, valid) if callable(fct) else fct
        metrics(reg, train, valid, fct.__name__)


if __name__ == '__main__':
    train_models()
