import sklearn.metrics as skmetrics
from sklearn.metrics import *

from ..features.common import Loggifier
from . import *


def exp_error(truth, preds, sample_weight=None, base=mean_absolute_error):
    log = Loggifier(errors='sign')
    truth = log.inverse_transform(truth)
    preds = log.inverse_transform(preds)
    return base(truth, preds, sample_weight=sample_weight)


class PerformanceMetric:
    def __init__(self, base):
        store_attr()
        self.__name__ = self.base.__name__

    def __call__(self, truth, preds, ds):
        return self.base(truth, preds, sample_weight=getattr(ds, 'sample_weight', None))

    @classmethod
    def create(self, identifier):
        if isinstance(identifier, str):
            return self.from_str(identifier)
        elif isinstance(identifier, dict):
            return self.from_dict(identifier)
        elif hasattr(identifier, '__call__'):
            return self(identifier)
        else:
            raise RuntimeError('Cant create PerformanceMetric')

    @classmethod
    def from_dict(self, metric):
        assert isinstance(metric, dict)
        if isinstance(metric['opts']['base'], str   ):
            metric['opts']['base'] = get_metric(metric['opts']['base'])
        mods = [skmetrics, sys.modules[__name__]]
        for m in mods:
            try:
                clf = getattr(m, metric['class'])
                clf = clf(**metric.get('opts', {}))
                return clf
            except AttributeError:
                pass
        raise KeyError('Unknown metric : ' + m)

    @classmethod
    def from_str(self, metric):
        assert isinstance(metric, str)
        return self(get_metric(metric))


def get_metric(metric):
    assert isinstance(metric, str)
    mods = [skmetrics, sys.modules[__name__]]
    for m in mods:
        try:
            return getattr(m, metric)
        except AttributeError:
            pass
    raise KeyError('Unknown metric : ' + m)


class PerformanceRelative(PerformanceMetric):
    def __call__(self, truth, preds, ds):
        assert hasattr(ds, 'relative_reweight')
        truth = truth * ds.relative_reweight
        preds = preds * ds.relative_reweight
        return self.base(truth, preds, sample_weight=getattr(ds, 'sample_weight', None))
