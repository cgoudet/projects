from copy import copy

import scipy.cluster.hierarchy as sch
from scipy.stats import spearmanr
from sklearn.cluster import DBSCAN

from src import *
from src.models.common import *
from src.models.metrics import PerformanceMetric

INDIR = ROOTDIR / 'references' / 'compare_models'


class Dataset:
    def __init__(self, X, y, sample_weight=None, weight_metric=None):
        store_attr()


class PerformanceDataset(object):
    def __init__(self, train_df, valid_df=None, target='defaillance_1a', columns=[], attrs=[]):
        numerics = ['int8', 'int16', 'int32', 'int64', 'float16', 'float32', 'float64', 'bool']
        train_df = train_df.select_dtypes(include=numerics)

        self.col_ind = addl(columns or train_df.columns, [target]+[x[1] for x in attrs])

        check_cols = addl(self.col_ind, [target])
        check_missings(train_df, check_cols)
        if valid_df is not None:
            check_missings(valid_df, check_cols)

        self.attrs = attrs
        self.target = target
        self.y_train = np.float32(train_df[target])
        self.fold_train = self.create_fold(train_df)

        self.train = train_df[self.col_ind].to_numpy(np.float32, copy=True)

        if valid_df is None:
            self.y_valid = self.y_train
            self.valid = self.train
            self.fold_valid = self.fold_train
        else:
            self.y_valid = np.float32(valid_df[target])
            self.valid = valid_df[self.col_ind].to_numpy(np.float32, copy=True)
            self.fold_valid = self.create_fold(valid_df)

    @staticmethod
    def create_fold(df):
        np.random.seed(53)
        if 'kfold' in df.columns:
            return np.int8(df['kfold'] % 5)
        else:
            return np.int8(np.random.randint(0, 5, size=len(df)))

    def assert_features_exist(self, features):
        missings = diffl(features, self.col_ind)
        if missings:
            raise KeyError('unknown features : {}'.format(','.join(missings)))

    def get_x_y(self, features, train=True, fold=0, as_frame=False):
        df, y, fold_value = ((self.train, self.y_train, self.fold_train)
                             if train else
                             (self.valid, self.y_valid, self.fold_valid))

        features_ind = [i for i, c in enumerate(self.col_ind) if c in set(features)]
        ind = np.where(train*(fold_value != fold)+(1-train)*(fold_value == fold))[0]

        out = df[ind][:, features_ind]
        y = y[ind]
        ds = Dataset(out, y)
        for name, col in self.attrs:
            col_num = self.col_ind.index(col)
            setattr(ds, name, df[ind, col_num])

        if as_frame:
            out = pd.DataFrame(out, columns=features)
            out[self.target] = y
            return out
        return ds

    @classmethod
    def from_fn(cls, train_fn, valid_fn=None, columns=None, **kwargs):
        train_df, valid_df = [cls.read_from_str(x, columns=columns) if x else None for x in [train_fn, valid_fn]]
        return cls(train_df, valid_df, columns=columns, **kwargs)

    @staticmethod
    def read_from_str(fn, columns):
        if fn == 'cancer':
            return pd.concat(load_breast_cancer(return_X_y=True, as_frame=True), axis=1)
        elif str(fn).endswith('.parquet'):
            return pd.read_parquet(fn, columns=None if columns is None else list(columns))
        elif str(fn).endswith('.csv'):
            return pd.read_csv(fn)
        else:
            raise RuntimeError('Format not implemented')


class PerformanceEstimator(object):
    def __init__(self, ds, folds=range(5), states=range(40, 45), verbose=0,
                 metrics=[], savedir=ROOTDIR / 'data' / 'interim'):
        store_attr()
        self.savedir.mkdir(exist_ok=True, parents=True)
        self.metrics = [x if isinstance(x, PerformanceMetric) else PerformanceMetric.create(x) for x in self.metrics]

    def _get_preds(self, clf, X):
        if 'Classifier' in clf.__class__.__name__:
            return clf.predict_proba(X)[:, 1]
        else:
            return clf.predict(X)

    def _metrics(self, clf, ds):
        preds = self._get_preds(clf, ds.X)
        return {m.__name__:  m(ds.y, preds, ds)
                for m in self.metrics}

    def train_single(self, train, valid, state, clf_type='RandomForestClassifier', clf_opt={},
                     fit_params={}):
        clf_opt = copy(clf_opt)
        clf_opt['random_state'] = state
        clf = init_clf(clf_type, clf_opt)

        if 'early_stopping_rounds' in fit_params:
            fit_params['eval_set'] = [(valid.X, valid.y)]
            fit_params['eval_names'] = ['valid']

        start = time.time()
        clf.fit(train.X, train.y, sample_weight=train.sample_weight, **fit_params)
        end = time.time()

        metrics = {k+('_valid' if i else '_train'): v
                   for i, df in enumerate([train, valid])
                   for k, v in self._metrics(clf, df).items()}
        metrics['duration'] = end-start
        metrics['size'] = pickle_size(clf)
        clf.metrics = metrics
        return clf

    def read_or_evaluate(self, savename, **kwargs):
        try:
            return pd.read_csv(self.savedir / f'{savename}.csv').set_index('metric')
        except FileNotFoundError:
            pass
        return self.evaluate(savename=savename, **kwargs)

    def evaluate(self, features, savename=None, saveinfo={}, **kwargs):
        self.ds.assert_features_exist(features)
        features = sorted(features)

        metrics = []
        for fold in self.folds:
            valid, train = [self.ds.get_x_y(features, train=do_train, fold=fold)
                            for do_train in range(2)]

            for i in self.states:
                clf = self.train_single(train, valid, i, **kwargs)
                clf.features = features
                metrics.append(clf.metrics)

                if self.verbose:
                    self.print_metrics(fold, i, clf)

        metrics = self._metrics_output(metrics)
        if savename:
            clf.metrics.update({'fold': fold, **saveinfo})
            self.save_model(clf, savename)
            fn = self.savedir / f'{savename}.csv'
            print('saving to', fn)
            metrics.to_csv(fn)
            np.save(fn.with_suffix('.npy'), self._get_preds(clf, valid.X))

        return metrics

    def print_metrics(self, fold, i, clf):
        metrics_msg = [f'{k}={np.round(clf.metrics[k], 5)}' for k in sorted(clf.metrics.keys())]
        print('fold={}\tstate={}\t{}'.format(fold, i, '\t'.join(metrics_msg)))

    def _metrics_output(self, metrics):
        metrics = pd.DataFrame.from_records(metrics)
        m = metrics.mean(0)
        s = metrics.std(0)
        out = pd.concat([m, s], axis=1).sort_index()
        out.columns = ['mean', 'std']
        out.index.name = 'metric'
        return out

    def save_model(self, clf, name, path=None):
        fn = Path(path or self.savedir) / f'{name}.joblib'
        print('saving', fn)
        joblib.dump(clf, fn)

    def compare_single_change(self, to_test, features, **kwargs):
        to_test, features = sorted(to_test), sorted(features)
        models = {}

        for name in to_test:
            tmp_features = features
            if name == '_baseline':
                action = 'baseline'
            elif name in features:
                action = 'remove'
                tmp_features = diffl(features, [name])
            else:
                action = 'add'
                tmp_features = addl(features, [name])
            models[name] = {'features': tmp_features,
                            'action': action}
        return self.compare_models(models, **kwargs)

    def compare_models(self, models, clf_opt={}, clf_type="RandomForestClassifier", savename=None,
                       read=False, **kwargs):
        all_features = chain(*[params['features'] for _, params in models.items()])
        self.ds.assert_features_exist(all_features)
        perfs = []
        fct = self.read_or_evaluate if read else self.evaluate
        for i, (name, params) in enumerate(models.items()):
            print(f'training model {i+1}/{len(models)} {name}')
            saveinfo = {k: params[k] for k in diffl(params.keys(),
                                                    ['features', 'clf_opt', 'clf_type', 'fit_params'])}
            saveinfo['name'] = name
            tmp_savename = None if savename is None else '_'.join([savename, name])
            score = fct(
                features=params['features'],
                clf_opt=params.get('clf_opt', clf_opt),
                savename=tmp_savename,
                saveinfo=saveinfo,
                clf_type=params.get('clf_type', clf_type),
                **kwargs)
            score = score['mean'].to_dict()
            score.update(saveinfo)
            print(f'model {name} {score}')
            perfs.append(score)

        return pd.DataFrame(perfs)


def tabular_embeddings(learn):
    out = {}
    for i, (varname, labels) in enumerate(learn.dls.procs.categorify.classes.items()):
        weights = learn.model.embeds[i].weight
        matching = pd.DataFrame(weights.detach().numpy(), columns=[
                                f'emb_{varname}_{d}' for d in range(weights.size(1))])
        matching['label'] = labels
        out[varname] = matching
    return out


def save_embeddings(embs, path, prefix=''):
    for varname, matching in embs.items():
        fn = path / 'embedding{}_{}_{}D.csv'.format('' if not prefix else '_'+prefix, varname, matching.shape[1]-1)
        matching.to_csv(fn, index=False)


def show_embeddings(embs, meta=None):
    ndim = embs.shape[1]-1

    if meta:
        meta = pd.read_csv(ROOTDIR / 'references' / 'matching' / meta)
        meta['value'] = meta['value'].str.replace("'", "")
        embs = pd.merge(embs, meta, left_on=['label'], right_on=meta.columns[0])

    labels = np.unique(embs['value']) if isinstance(meta, pd.DataFrame) else ['label']
    fig, axes = plt.subplots(nrows=ndim-1)
    if ndim == 2:
        axes = [axes]
    for label in labels:
        mask = (embs['value'] == label) if isinstance(meta, pd.DataFrame) else np.ones(len(embs), dtype=bool)
        tmp = embs[mask]
        for i, ax in enumerate(axes):
            ax.scatter(tmp[embs.columns[0]], tmp[embs.columns[i+1]], alpha=0.4, label=label)


def drop_correlated(corr):
    dbscan = DBSCAN(eps=0.2, min_samples=2, n_jobs=30, metric='precomputed')
    labels = dbscan.fit_predict(1-np.abs(corr))
    uncorr = pd.DataFrame(zip(corr.columns, labels),
                          columns=['varname', 'label'])
    no_group = uncorr['label'] == -1
    uncorr.loc[no_group, 'label'] = -np.arange(no_group.sum())-1
    uncorr.drop_duplicates(subset=['label'], inplace=True)
    return sorted(uncorr.varname)


def get_spearman(ds, suffix, read=True):
    fn = ROOTDIR / 'data' / 'interim' / f'correlations_{suffix}.csv'
    if read:
        corr = pd.read_csv(fn, index_col=0)

    else:
        corr, _ = spearmanr(ds.train, axis=0)
        cols, _ = zip(*sorted([(name, ind) for name, ind in ds.col_ind.items()], key=lambda x: x[1]))
        corr = pd.DataFrame(corr, columns=cols, index=cols)
        corr.to_csv(fn)

    to_remove = corr.isnull().all()
    to_remove = to_remove[to_remove].index.values
    corr = corr.drop(columns=to_remove).drop(to_remove)
    return corr


def get_best_from_clusters(corr, clst_sz, imps, link=None):
    if not link:
        link = sch.linkage(1-np.abs(corr), method="ward")

    assert clst_sz > 0
    n_feat = np.round(clst_sz*len(imps)) if clst_sz < 1 else int(clst_sz)
    assert n_feat < len(imps)

    labels = sch.fcluster(link, n_feat, criterion='maxclust')

    features = pd.DataFrame(zip(corr.columns, labels), columns=['name', 'label'])
    features = (pd.merge(features, imps, how='left', on='name')
                .sort_values(['label', 'importance'], ascending=[True, False])
                .drop_duplicates(subset=['label'])
                )
    return sorted(features.name)


def check_perf_clusters(ds, corr, clst_szs, imps, clf_opt={}):
    """Test les performances des N variables les plus indépendantes"""
    perfs = []
    link = sch.linkage(1-np.abs(corr), method="ward")
    for t in clst_szs:
        assert t > 0
        n_feat = np.round(t*len(imps)) if t < 1 else int(t)
        assert n_feat < len(imps)
        labels = sch.fcluster(link, n_feat, criterion='maxclust')

        features = pd.DataFrame(zip(corr.columns, labels), columns=['name', 'label'])
        features = (pd.merge(features, imps, how='left', on='name')
                    .sort_values(['label', 'importance'], ascending=[True, False])
                    .drop_duplicates(subset=['label'])
                    )
        score = ds.perfs_fold(features.name, states=range(40, 41),
                              clf_opt=clf_opt)
        print(score)
        score = score[0]
        perfs.append((t, n_feat, score))

    perfs_df = pd.DataFrame(perfs, columns=['clst_szs', 'n_feat', 'score'])
    return perfs_df


def read_loo(prefix):
    path = ROOTDIR / 'data' / 'interim'
    fns = list(path.glob(f'{prefix}_*.joblib'))
    if not fns:
        raise FileNotFoundError(prefix)
    print('num_files', len(fns))
    df = pd.DataFrame([joblib.load(fn).metrics for fn in fns]).sort_values('roc_auc_score_valid')
    return df


def make_compare_plot(perfs, savename):
    perfs = perfs.copy(deep=True)
    perfs.sort_values('roc_auc_score_valid', ascending=True, inplace=True)
    perfs['roc_auc_score_valid'] *= 100

    relative = '_baseline' in list(perfs.name)
    if relative:
        baseline = perfs[perfs.name == '_baseline'].iloc[0]
        perfs['roc_auc_score_valid'] -= baseline['roc_auc_score_valid']
        perfs = perfs[perfs['name'] != '_baseline']

    fig, ax = plt.subplots()
    ax.barh(perfs.name, perfs.roc_auc_score_valid, color='cornflowerblue')
    ax.set_xlabel('roc_auc_score (%)')
    if not relative:
        ax.set_xlim(np.floor(perfs.roc_auc_score_valid.min()), np.ceil(perfs.roc_auc_score_valid.max()))

    title = "Performance{} du modèle : {}".format(' relative' if relative else "", savename.stem)
    ax.set_title(title, loc='left')

    fig.savefig(savename, bbox_inches='tight')


@tracker(None, inputs=True, log_start=True)
def compare_models(step, models=None, savedir=None, configdir=None, read=False):
    configdir = Path(configdir or INDIR)
    with (configdir / step).with_suffix('.json').open() as f:
        config = json.load(f)
    mdls = models_from_config(config)
    if models:
        mdls = {k: mdls[k] for k in models}

    all_features = addl(set(chain(*[params['features'] for _, params in mdls.items()])),
                        [config['target']] + [x[1] for x in config.get('attrs', [])])
    ds = PerformanceDataset.from_fn(config['train_fn'], config.get('valid_fn'),
                                    target=config["target"],
                                    columns=all_features,
                                    attrs=config.get('attrs', []))
    perfestim = PerformanceEstimator(ds, **config.get('perfestim_opt', {}))

    perfs_df = perfestim.compare_models(mdls, savename=step, read=read)

    savedir = Path(savedir or ROOTDIR / 'data' / 'interim' / 'compare_models')
    savedir.mkdir(exist_ok=True, parents=True)
    for d in perfs_df.to_dict(orient='records'):
        with (savedir / '{}_{}.json'.format(step, d['name'])).open('w') as f:
            json.dump(d, f)

    return perfs_df


def parse_baseline_features(config):
    baseline_features = read_external(config, 'baseline_features') or []
    all_drop = [mdl.get('drop_features', []) for _, mdl in config.get('models', {}).items()]
    baseline_features = addl(baseline_features, list(chain(*all_drop)))

    all_additions = [mdl.get('features', []) for _, mdl in config.get('models', {}).items()]
    baseline_features = diffl(baseline_features, list(chain(*all_additions)))

    return baseline_features


def parse_baseline_clf_opt(config):
    clf_opt = read_external(config, "baseline_clf_opt", 'get_params')
    if not clf_opt:
        return {}
    if isinstance(clf_opt, dict):
        return clf_opt
    return clf_opt()


def models_from_config(config):
    mdls = config.get('models', {})
    mdls = {k: read_external(mdls, k) for k in mdls}
    config['models'] = mdls
    baseline_clf_opt = parse_baseline_clf_opt(config)
    baseline_features = parse_baseline_features(config)
    # if baseline_features:
    #     mdls['_baseline'] = {'features': baseline_features}

    for k in config['models']:
        mdls[k]['features'] = addl(mdls[k].get('features', []), baseline_features)
        mdls[k]['features'] = diffl(mdls[k].get('features', []), mdls[k].get('drop_features', []))
        mdls[k]['clf_opt'] = {**baseline_clf_opt, **mdls[k].get('clf_opt', {})}
    return mdls


def plot_models_perfs(step):
    indir = ROOTDIR / 'data' / 'interim' / 'compare_models'
    all_models = list(indir.glob(f'{step}_*.json'))
    if not all_models:
        raise RuntimeError('No models')
    data = []
    for fn in all_models:
        with open(fn) as f:
            data.append(json.load(f))
    perfs_df = pd.DataFrame(data)

    savename = ROOTDIR / 'reports' / 'figures' / 'compare_models' / f'{step}.png'
    make_compare_plot(perfs_df, savename)
    return perfs_df
