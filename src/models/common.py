import lightgbm as lgb
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.linear_model import LinearRegression

from ..features.common import *
from . import *


def init_clf(clf_type, clf_opt={}):
    match = {'RandomForestClassifier': RandomForestClassifier,
             'RandomForestRegressor': RandomForestRegressor,
             'LGBMClassifier': lgb.LGBMClassifier,
             'LinearRegression': LinearRegression,
             'LGBMRegressor': lgb.LGBMRegressor,
             'MedianRegressor': MedianRegressor,
             'LastYear': LastYear,
             'LogRegressor': LogRegressor.from_elements}
    return match[clf_type](**clf_opt)


class MeanStacking(BaseEstimator, RegressorMixin):
    def __init__(self, mdls):
        store_attr()

    def fit(self, X, y=None):
        return self

    def predict(self, X):
        preds = np.array([mdl.predict(np.array(X[mdl.features])) for mdl in self.mdls]).T
        return preds.mean(axis=1)

    @classmethod
    def from_names(self, mdls_names):
        mdls = [joblib.load(fn) for fn in mdls_names]
        return self(mdls)


class LogRegressor(BaseEstimator, RegressorMixin):
    def __init__(self, base, **kwargs):
        self.base_ = base
        self.log_ = Loggifier(**kwargs)

    def fit(self, X, y=None, sample_weight=None):
        y = self.log_.fit_transform(np.array(y).reshape(-1, 1)).reshape(-1)
        self.base_.fit(X, y, sample_weight=sample_weight)
        return self

    def predict(self, X):
        preds = self.base_.predict(X)
        preds = self.log_.inverse_transform(preds.reshape(-1, 1)).reshape(-1)
        return preds

    @classmethod
    def from_elements(self, base_class, base_opt={}, log_opt={}, **kwargs):
        base = init_clf(base_class, base_opt)
        return self(base, **log_opt)


class MedianRegressor(BaseEstimator, RegressorMixin):
    def __init__(self, groups, fillna=0, **kwargs):
        store_attr(['groups', 'fillna'])

    def _to_frame(self, X, y=None):
        if isinstance(X, pd.DataFrame):
            X.columns = list(range(X.shape[1]))

        if y is not None:
            y = pd.DataFrame({'target': y})
        X = pd.DataFrame(X)
        return X, y

    def fit(self, X, y=None, sample_weight=None):
        df = pd.concat(self._to_frame(X, y), axis=1)
        df = df.astype({i: int for i in self.groups})

        self.avg = df.groupby(self.groups).agg({'target': np.median})
        self.avg.columns = ['preds']
        return self

    def predict(self, X):
        X, _ = self._to_frame(X)
        X = X[self.groups].astype(np.int64)
        out = (pd.merge(X, self.avg, left_on=self.groups, right_index=True, how='left')
               .fillna({'preds': self.fillna}))
        return out['preds'].values


class LastYear(MedianRegressor):
    def __init__(self, groups, step_col, fillna=0, **kwargs):
        store_attr(['fillna', 'step_col'])
        self.groups = groups + [step_col]

    def fit(self, X, y=None, sample_weight=None):
        self.avg = (pd.concat(self._to_frame(X, y), axis=1)
                    .astype({i: int for i in self.groups})
                    .drop_duplicates(subset=self.groups, keep='last')
                    .rename(columns={'target': 'preds'}))
        self.avg[self.step_col] += 1
        self.avg = self.avg.set_index(self.groups)
        self.pipe_ = 1
        return self
