import hashlib
import json
from datetime import date, datetime

import dateutil.parser as dparser
import pandas as pd
from fastcore.utils import ifnone
from sqlalchemy import Column, Date, ForeignKey, Integer, String, Text

from ...app import DB
from ...app.models import save_multiple


class RSSFeed(DB.Model):
    __tablename__ = "rss_feed"
    __bind_key__ = "presse"
    id = Column(Integer, primary_key=True)
    name = Column(String(64), unique=True)
    url = Column(String(256), unique=True)
    contents = DB.relationship("RSSContent", backref="feed", lazy="dynamic")

    @classmethod
    def add_from_csv(self, fn, **kwargs):
        df = pd.read_csv(fn, **kwargs)[["name", "url"]]

        feeds = [self(name=name, url=url) for name, url in df.values]
        save_multiple(feeds)

    def __repr__(self):
        return "<{} {} {} {}>".format(
            self.__class__.__name__, self.id, self.name, self.url
        )


class RSSContent(DB.Model):
    __tablename__ = "rss_content"
    __bind_key__ = "presse"

    url = Column(String, primary_key=True)
    content = Column(Text)
    fetch_dt = Column(Date, default=datetime.utcnow)
    feed_id = Column(Integer, ForeignKey("rss_feed.id"))

    def __repr__(self):
        return "<{} {}>".format(self.__class__.__name__, self.url)

    @classmethod
    def insert_raw(self, content, feed=None, fetch_dt=None):
        self.parse_rss_feed_exceptions(content)

        fetch_dt = ifnone(fetch_dt, pd.Timestamp.utcnow().date())

        contents = [
            self.from_entry(entry, fetch_dt, feed) for entry in content["entries"]
        ]
        contents = [x for x in contents if x is not None]
        save_multiple(contents)

    @classmethod
    def from_entry(self, entry, fetch_dt, feed=None):
        content = json.dumps(entry)
        url = entry.get("link", hashlib.sha224(content.encode("utf-8")).hexdigest())
        return self(
            url=url,
            content=content,
            fetch_dt=fetch_dt,
            feed_id=feed.id if feed else None,
        )

    @staticmethod
    def parse_rss_feed_exceptions(feed):
        key = "bozo_exception"
        bozo_except = feed.get(key)
        if not bozo_except:
            return
        if not isinstance(bozo_except, str):
            feed[key] = type(bozo_except).__name__ + " : " + str(bozo_except)

    def parse(self):
        try:
            article = json.loads(self.content)
            self.title = self._title(article)
            self.summary = self._summary(article)
            self.dt = self._dt(article)
        except Exception as e:
            print(self.content)
            raise
        return self

    def _title(self, article: dict):
        if not self.url.startswith("http"):
            return
        return article.get("title")

    def _summary(self, article: dict):
        keys = {"summary", "text"}
        present = sorted(keys & set(article.keys()))
        if not present:
            return
        return article[present[0]]

    def _dt(self, article: dict):
        if not self.url.startswith("http"):
            return
        keys = {"published_parsed", "updated_parsed"}
        present = sorted(keys & set(k for k, v in article.items() if v))
        if not present:
            return
        print(article)
        print("present", present)
        print(article[present[0]])
        return date(*article[present[0]][:3])
