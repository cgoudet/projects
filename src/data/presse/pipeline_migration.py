import json
import re

import pandas as pd
from fastprogress.fastprogress import progress_bar

from ... import ROOTDIR
from ...app import create_app
from .models import RSSContent, RSSFeed


def migrate_heng():
    feed = RSSFeed.query.filter_by(name="heng").first()
    fetch_dt = pd.Timestamp("2019-01-01").date()
    rows = read_heng_defaut().to_dict(orient="records")
    content = {"entries": [{"text": row["text"], "url": row["link"]} for row in rows]}
    RSSContent.insert_raw(content, feed=feed, fetch_dt=fetch_dt)


def read_rss_feeds_list():
    feeds = pd.read_csv(
        ROOTDIR / "references" / "presse" / "list_flux_rss.csv", sep=";"
    )
    tm = pd.Timestamp.utcnow().strftime("%Y%m%d")
    feeds["outfn"] = squeeze(feeds.name) + f"_{tm}"
    feeds.sort_values("outfn", inplace=True)
    counts = feeds.outfn.value_counts()
    assert len(counts[counts > 1]) == 0
    return feeds


def squeeze(serie):
    names = (
        serie.str.lower().str.replace("[^\w\s]", "", regex=True).str.replace(" ", "")
    )
    names = (
        names.str.normalize("NFKD")
        .str.encode("ascii", errors="ignore")
        .str.decode("utf-8")
    )
    return names


def read_heng_defaut():
    fn = ROOTDIR / "data" / "raw" / "presse" / "heng_defaut.csv"
    df = pd.read_csv(fn, sep=";")
    df.rename(columns={"content": "text", "label": "is_defaut"}, inplace=True)
    df["link"] = ["heng_" + str(x) for x in range(len(df))]
    return df


def migrate_manual(fn, feed, fetch_dt):
    with fn.open() as f:
        txt = f.read()
    content = {"entries": [{"text": txt, "url": fn.stem}]}
    RSSContent.insert_raw(content, feed=feed, fetch_dt=fetch_dt)


def migrate_all_manual():
    feed = RSSFeed.query.filter_by(name="manual").first()
    fetch_dt = pd.Timestamp("2020-03-01").date()

    manuals = list((ROOTDIR / "data" / "raw" / "presse" / "manual").glob("*.txt"))
    [migrate_manual(fn, feed, fetch_dt) for fn in progress_bar(manuals)]


def migrate_feeds():
    feeds = (
        pd.read_csv(ROOTDIR / "references" / "presse" / "list_flux_rss.csv", sep=";")
        .rename(columns={"link": "url"})
        .drop(columns=["type", "alt"])
    )
    manual = pd.DataFrame(
        [{"name": "manual", "url": "manual"}, {"name": "heng", "url": "heng"}]
    )
    feeds = pd.concat([feeds, manual], axis=0)
    fn = "/tmp/feeds.csv"
    feeds.to_csv(fn)
    RSSFeed.add_from_csv(fn)

    all_feeds = RSSFeed.query.all()
    return all_feeds


def migrate_content(size=None):
    feeds = match_feeds()

    prefix = ROOTDIR / "data" / "raw" / "presse" / "rss_feeds"
    fns = pd.DataFrame({"fn": sorted(prefix.glob("*.json"))})
    if size:
        fns = fns.iloc[:size]

    pat = re.compile(r"(.*)_(\d{8}).json")
    fns["pat"] = fns.fn.apply(lambda x: pat.match(x.name))
    fns["outfn"], fns["fetch_dt"] = zip(
        *fns.pat.apply(lambda x: (x.group(1), x.group(2)))
    )
    fns["fetch_dt"] = pd.to_datetime(fns.fetch_dt, format="%Y%m%d").dt.date
    del fns["pat"]

    comb = (
        pd.merge(feeds, fns, on="outfn", how="right")
        .sort_values(["fetch_dt", "outfn"])
        .drop(columns="name")
        .dropna(subset=["feed"])
        .to_dict(orient="records")
    )

    mb = progress_bar(comb)
    [migrate_from_dict(row) for row in mb]


def migrate_from_dict(row):
    with row["fn"].open() as f:
        content = json.load(f)
    try:
        RSSContent.insert_raw(
            content, feed=row.get("feed"), fetch_dt=row.get("fetch_dt")
        )
    except Exception:
        print(row)
        raise


def parse_rss_feed_exceptions(feed):
    key = "bozo_exception"
    bozo_except = feed.get(key)
    if not bozo_except:
        return
    if not isinstance(bozo_except, str):
        feed[key] = type(bozo_except).__name__ + " : " + str(bozo_except)


def match_feeds():
    feeds = RSSFeed.query.all()
    feeds = pd.DataFrame([(x, x.name) for x in feeds], columns=["feed", "name"])

    raw = read_rss_feeds_list()[["name", "outfn"]]
    raw.outfn = raw.outfn.apply(lambda x: x[: x.rfind("_")])

    comb = pd.merge(feeds, raw, on=["name"])
    return comb


if __name__ == "__main__":
    app = create_app("PROD")
    with app.app_context():
        migrate_feeds()
        migrate_heng()
        migrate_all_manual()
        migrate_content()
