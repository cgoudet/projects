import json
import re

import bs4
import feedparser
import furl
import numpy as np
import pandas as pd
from dateutil.parser import ParserError
from fastcore.utils import ifnone, parallel

from ... import ROOTDIR
from ...app import create_app
from .models import RSSContent, RSSFeed
from .pipeline_migration import read_heng_defaut, read_rss_feeds_list

RAWDIR = ROOTDIR / "data" / "raw" / "presse"
PRESSEDIR = RAWDIR


def save_all_rss_feeds(n_workers=10, config="DEV"):
    app = create_app(config)
    with app.app_context():
        feeds = np.array_split(
            [f for f in RSSFeed.query.all() if ifnone(f.url, "").startswith("http")],
            n_workers * 3,
        )
        parallel(save_multiple_rss_feeds, feeds, config=config, n_workers=n_workers)


def save_multiple_rss_feeds(feeds, config="DEV"):
    app = create_app(config)
    with app.app_context():
        [save_rss_feed(feed) for feed in feeds]


def save_rss_feed(feed):
    try:
        fetch_dt = pd.Timestamp.utcnow().date()
        content = feedparser.parse(feed.url)
        RSSContent.insert_raw(content, feed=feed, fetch_dt=fetch_dt)
    except Exception as e:
        print("error feed : ", feed, e)


def parse_rss_feed_exceptions(feed):
    key = "bozo_exception"
    bozo_except = feed.get(key)
    if not bozo_except:
        return
    if not isinstance(bozo_except, str):
        feed[key] = type(bozo_except).__name__ + " : " + str(bozo_except)


def match_feeds():
    feeds = RSSFeed.query.all()
    feeds = pd.DataFrame([(x, x.name) for x in feeds], columns=["feed", "name"])

    raw = read_rss_feeds_list()[["name", "outfn"]]
    raw.outfn = raw.outfn.apply(lambda x: x[: x.rfind("_")])

    comb = pd.merge(feeds, raw, on=["name"])
    return comb


def date_in_link(link):

    out = re.search(r"\d{2}[-/]\d{2}[-/]20\d{2}", link)
    if out:
        return out.group(0)

    out = re.search(r"20\d{2}[-/]\d{2}[-/]\d{2}", link)
    if out:
        return out.group(0)


def test_parsing(dt):
    try:
        return pd.to_datetime(dt, dayfirst=True).date()
    except ParserError:
        return
    except AttributeError:
        return


def date_from_rss(entry):
    possibles = ["published", "updated"]
    for k in possibles:
        out = test_parsing(entry.get(k))
        if out:
            return out

    out = test_parsing(date_in_link(entry.get("link", "")))
    if out:
        return out

    for link in entry.get("links", []):
        out = test_parsing(date_in_link(link["href"]))
        if out:
            return out
    return


def parse_entry(entry):
    try:
        out = {k: entry.get(k, "") for k in ["title", "link", "summary"]}
        if out["summary"]:
            out["summary"] = bs4.BeautifulSoup(
                out["summary"], features="lxml"
            ).text.strip()

        out["link"] = out["link"].replace("https://www.bfmtv.comhttps://", "https://")
        out["link"] = furl.furl(out["link"]).remove(args=True, fragment=True).url
        out["date"] = date_from_rss(entry)
        out["text"] = out["title"] + "\n" + out["summary"]
        return out

    except Exception:
        print("error_entry")
        print(entry)
        raise
        return {}


def parse_rss_feed(fn):
    content = json.load(fn.open())
    entries = content["entries"]
    return [{"feed": fn.stem, **parse_entry(entry)} for entry in entries]


def parse_all_rss_feeds(n_workers, pat=None):
    infiles = list((PRESSEDIR / "rss_feeds").glob("*.json"))
    if pat:
        if isinstance(pat, str):
            pat = re.compile(pat)
        infiles = [x for x in infiles if pat.match(x.name)]
    out = parallel(parse_rss_feed, infiles, n_workers=n_workers)
    df = pd.DataFrame(chain(*out))
    df.drop_duplicates(subset=["link"], inplace=True)
    print("feeds shape", df.shape)
    return df


def data_from_file(fn, index=0):
    with fn.open() as f:
        text = f.read().strip()
    return text, fn.stem


def title_summary(s):
    if pd.isna(s):
        return None, None
    s = s.split("\n")
    return s[0], "\n".join(s[1:])


def read_manuel(n_workers=1):
    all_files = list((PRESSEDIR / "manual").glob("*.txt"))
    df = pd.DataFrame(
        parallel(data_from_file, all_files, n_workers=n_workers),
        columns=["text", "name"],
    )
    print("manual", df.shape, len(all_files))
    df["title"], df["summary"] = zip(*df.text.apply(title_summary))
    df["feed"] = "manuel"
    df["link"] = df["name"]

    to_drop = utilf.diffl(df.columns, ["feed", "title", "link", "summary"])
    df.drop(columns=to_drop, inplace=True)
    return df


def load_all_summaries(n_workers=10, pat=None):
    old_rss = pd.read_csv(PRESSEDIR / "old_rss.csv", sep=";", encoding="utf-8")
    print("old_rss_shape", old_rss.shape)
    manuel = read_manuel()
    feeds = parse_all_rss_feeds(n_workers, pat=pat)
    heng = read_heng_defaut().rename(columns={"text": "title"})
    news = pd.read_parquet(PRESSEDIR / "news.parquet")
    print("news_shape", news.shape)
    targets = pd.read_csv(PRESSEDIR / "targets.csv", sep=";")
    print(manuel.head())
    print(old_rss.head())
    print(feeds.head())
    print(heng.head())
    print(news.head())
    news = pd.concat(
        [manuel, old_rss, feeds, heng, news], axis=0, ignore_index=True
    ).drop_duplicates(subset=["link"])
    news = pd.merge(news, targets, on="link", how="left")
    news.loc[news.feed == "manuel", "label"] = "FRAUDE_ENT"
    news["text"] = news.title + "\n" + news.summary.fillna("")
    news["is_fraude"] = (news.label == "FRAUDE_ENT").fillna(0).astype(np.int8)
    to_keep = ["link", "text", "label"]
    to_drop = sorted(set(news.columns) - set(to_keep))
    news.drop(columns=to_drop, inplace=True)
    news.to_parquet(PRESSEDIR / "presse.parquet")
    return news


def parse_news(fn):
    with fn.open() as f:
        content = json.load(f)

    out = {
        "title": content["title"],
        "link": furl.furl(content["url"]).remove(args=True, fragment=True).url,
        "summary": content.get("text", "").strip(),
        "feed": fn.stem,
    }
    return out


def parse_all_news(n_workers=10):
    all_files = list((PRESSEDIR / "news").glob("*.json"))
    out = parallel(parse_news, all_files, n_workers=n_workers)
    df = pd.DataFrame(out)
    df.drop(df[df.link.str.contains("anibis.ch")].index, inplace=True)
    df.drop(df[df.link.str.contains("http://questions.sncf.com/")].index, inplace=True)
    df["text"] = df["title"] + "\n" + df["summary"]
    df.to_parquet(PRESSEDIR / "news.parquet")
    return df


if __name__ == "__main__":
    save_all_rss_feeds(n_workers=10, config="PROD")
