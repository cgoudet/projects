from . import *


def limit_int(df):
    int_cols = list(df.select_dtypes('integer').columns)
    encodings = [np.int8, np.int16, np.int32]
    for c in int_cols:
        m = np.abs(df[c]).max()
        for e in encodings:
            if m < np.iinfo(e).max:
                df[c] = e(df[c])
                break


def limit_types(df):
    dates = [k for k, v in df.dtypes.to_dict().items() if str(v) == 'datetime64[ns]']
    for c in dates:
        df.loc[:, c] = df[c].astype('datetime64[s]')

    f64 = [k for k, v in df.dtypes.to_dict().items() if str(v) == 'float64']
    for c in f64:
        df.loc[:, c] = df[c].astype(np.float32)

    limit_int(df)
    return df


def add_missing_dates(df, groups, col_date='date'):
    all_groups = df[groups].drop_duplicates().values
    all_dates = np.unique(df[col_date])
    cols = [col_date] + groups
    all_groups_dates = pd.DataFrame([[dt] + list(g)
                                     for dt, g in product(all_dates, all_groups)],
                                    columns=cols)
    df = pd.merge(all_groups_dates, df, on=cols, how='left')
    print(df.shape)

    return df
