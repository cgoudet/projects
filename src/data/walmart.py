from sklearn.impute import KNNImputer
from sklearn.neighbors import KNeighborsRegressor

from ..features.common import *
from . import *
from .common import *

SAVEDIR = ROOTDIR / 'data' / 'interim' / 'walmart'


def add_transformer_features(df, step='train'):
    fn = SAVEDIR / 'transformers{}.joblib'.format('_full' if 'full' in step else '')
    emb_store_fn = SAVEDIR / 'emb_correlation_store.csv'
    if step in ['train', 'full']:
        agg_serie_opts = dict(target='weekly_sales',
                              target_fct=np.median, group_fct=np.sum,
                              prefix='median_weekly_sales_')
        opts = dict(
            extra=[('median_weekly_sales_store',
                    AggSerieValue(main_cols=['store'], **agg_serie_opts),
                    ['store', 'date', 'weekly_sales']),
                   ('median_weekly_sales_store_dept',
                    AggSerieValue(main_cols=['store', 'dept'], **agg_serie_opts),
                    ['store', 'dept', 'date', 'weekly_sales']),
                   ('median_weekly_sales_dept',
                    AggSerieValue(main_cols=['dept'], **agg_serie_opts),
                    ['dept', 'date', 'weekly_sales']),
                   ('emb_store_correlation',
                    FrameMapper.from_csv(emb_store_fn, ['store']),
                    ['store']),
                   ('emb_dept_correlation',
                    FrameMapper.from_csv(SAVEDIR / 'emb_correlation_dept.csv', ['dept']),
                    ['dept']),
                   ('emb_store_dept_correlation',
                    FrameMapper.from_csv(SAVEDIR / 'emb_correlation_store_dept.csv', ['store', 'dept']),
                    ['store', 'dept'])
                   ],
            date_featurer=['date'],
            date_featurer_opt=dict(attrs=['week', 'month', 'dayofyear', 'cos_dayinyear', 'sin_dayinyear', 'year']),
            loggifier=['weekly_sales'],
            loggifier_opt=dict(errors='sign'),
        )
        tf = GeneralEncoder(**opts).fit(df)
        joblib.dump(tf, fn)
    else:
        tf = joblib.load(fn)

    out = pd.concat([df, tf.transform(df)], axis=1)
    # out['relative_weekly_sales'] = out['weekly_sales'] / out['median_weekly_sales_store']
    # out['log_median_weekly_sales_store'] = np.log10(out['median_weekly_sales_store'])

    return out


def add_join_features(df, step='train'):
    df = df.copy()
    df['kfold'] = int(step in ['train', 'full'])
    df['sample_weight'] = 1+df['is_holiday']*4
    df['relative_weekly_sales'] = df['weekly_sales'] / df['median_weekly_sales_store_dept']
    df['diff_relative_weekly_sales'] = np.where(df['median_weekly_sales_store_dept'] <= 0, 0,
                                                (df['weekly_sales'] - df['last_week_weekly_sales'])
                                                / df['median_weekly_sales_store_dept'])

    df['diff_year_relative_weekly_sales'] = np.where(np.abs(df['median_weekly_sales_store_dept']) > 100,
                                                     (df['weekly_sales'] - df['last_year_weekly_sales']) /
                                                     df['median_weekly_sales_store_dept'],
                                                     np.nan)

    df['relative_year_weekly_sales'] = np.where(np.abs(df['last_year_weekly_sales']) > 100,
                                                df['weekly_sales'] / df['last_year_weekly_sales'], np.nan)
    return df


def fillna_emb_store_dept(X, step='train'):
    fn = SAVEDIR / 'fillna_dept_store.joblib'

    X['extended_dept'] = X['dept']*10
    if step in ['train', 'full']:
        tf = FillNAClosest(emb_cols=['emb_correlation_store_0', 'emb_correlation_store_1', 'extended_dept'],
                           n_neighbors=3,
                           targets=['emb_correlation_store_dept_0', 'emb_correlation_store_dept_1'])
        tf.fit(X)
        joblib.dump(tf, fn)
    else:
        tf = joblib.load(fn)

    X = (tf.transform(X)
         .drop(columns=['extended_dept'])
         .fillna({'median_weekly_sales_store_dept': 0}))
    return X


def add_lag(df, read_match=True):
    if read_match:
        fn = ROOTDIR / 'data' / 'interim' / 'walmart' / 'walmart_full.parquet'
        match = pd.read_parquet(fn)
    else:
        match = df.copy()

    match_year = (match[['store', 'dept', 'week', 'year', 'weekly_sales']]
                  .rename(columns={'weekly_sales': 'last_year_weekly_sales'}))
    match_year['year'] = match_year['year'] + 1
    df = pd.merge(df, match_year, on=['store', 'dept', 'week', 'year'], how='left')

    match_week = (match[['store', 'dept', 'date', 'weekly_sales']]
                  .rename(columns={'weekly_sales': 'last_week_weekly_sales'}))
    match_week['date'] = match_week['date'] + pd.DateOffset(days=7)
    df = pd.merge(df, match_week, on=['store', 'dept', 'date'], how='left')
    return df


def transform(step='train'):
    df = read(step).drop(columns=['is_holiday'])
    if step != 'test':
        df = (add_missing_dates(df, ['store', 'dept'])
              .fillna({'weekly_sales': 0}))

    train_valid_limit = pd.Timestamp('2011-11-01')
    if step == 'train':
        df = df[df.date < train_valid_limit]
    elif step == 'valid':
        df = df[df.date > train_valid_limit]

    df = add_store_info(df)
    df = add_transformer_features(df, step=step)
    df = fillna_emb_store_dept(df, step=step)
    df = add_lag(df, read_match=step != 'full')
    df = add_join_features(df, step=step)
    df = force_types(df)
    limit_types(df)
    outdir = ROOTDIR / 'data' / 'interim' / 'walmart'
    outdir.mkdir(exist_ok=True)
    df.to_parquet(outdir / f'walmart_{step}.parquet')


def force_types(df):
    to_int = ['week', 'store', 'dept']
    df = df.astype({c: np.int32 for c in to_int})
    return df


def add_store_info(df):
    rawdir = ROOTDIR / 'data' / 'raw' / 'walmart'
    store_info = (pd.read_csv(rawdir / 'features.csv.zip', parse_dates=['Date'])
                  .rename(columns={'IsHoliday': 'is_holiday'}))
    store_info.columns = [x.lower() for x in store_info.columns]
    store_info = store_info.drop(columns=[x for x in store_info.columns if 'markdown' in x])
    df = pd.merge(df, store_info, on=['store', 'date'], how='left')
    df = df[sorted(df.columns)]
    return df


def read(step='train'):
    assert step in ['train', 'valid', 'test', 'full', 'test_full']
    source = 'test' if 'test' in step else 'train'
    rawdir = ROOTDIR / 'data' / 'raw' / 'walmart'
    df = (pd.read_csv(rawdir / f'{source}.csv.zip', parse_dates=['Date'])
          .rename(columns={'IsHoliday': 'is_holiday'}))

    df.columns = [x.lower() for x in df.columns]
    df = df.sort_values(['store', 'dept', 'date'])
    if 'weekly_sales' not in df.columns:
        df['weekly_sales'] = 1
    return df


def pipeline():
    # for step in ['full']:
    for step in ['full', 'test_full', 'train', 'valid', 'test']:
        print(step)
        transform(step)


def embeddings():
    df = read('full')
    SAVEDIR = ROOTDIR / 'data' / 'interim' / 'walmart'
    opts = {'target': 'weekly_sales'}

    DateCorrelationEmbedding(['store'], **opts).fit(df).save(SAVEDIR / 'emb_correlation_store.csv')
    DateCorrelationEmbedding(['dept'], **opts).fit(df).save(SAVEDIR / 'emb_correlation_dept.csv')
    DateCorrelationEmbedding(['store', 'dept'], **opts).fit(df).save(SAVEDIR / 'emb_correlation_store_dept.csv')


class FillNAClosest(BaseEstimator, TransformerMixin):
    def __init__(self, emb_cols, targets, **kwargs):
        self.emb_cols = listify(emb_cols)
        self.targets = listify(targets)
        self.imp_ = KNeighborsRegressor(**kwargs)

    def fit(self, X, y=None):
        print(self.emb_cols+self.targets)
        X = (X[self.emb_cols+self.targets]
             .dropna()
             .drop_duplicates())
        self.imp_.fit(X[self.emb_cols], X[self.targets])
        return self

    def transform(self, X, y=None):
        out = pd.DataFrame(self.imp_.predict(X[self.emb_cols]),
                           columns=self.targets,
                           index=X.index)
        X = X.fillna({c: out[c] for c in out.columns})
        return X


def undiff_per_group(df, last_col='last_week_weekly_sales'):
    cols = ['store', 'dept', 'date']
    tmp = df[cols + ['preds', 'median_weekly_sales_store_dept', last_col]].copy()
    preds_sales = tmp['preds'] * tmp['median_weekly_sales_store_dept']
    preds_sales = preds_sales + np.where(tmp.date == tmp.date.min(), tmp[last_col], 0)
    tmp['preds_sales'] = preds_sales
    tmp = (tmp.set_index(cols)
           .groupby(level=[0, 1])
           .agg({'preds_sales': np.cumsum}))
    df = pd.merge(df, tmp, left_on=cols, right_index=True, how='left')
    return df


if __name__ == '__main__':
    # embeddings()
    pipeline()
