from fastai.text.all import *

from . import *

DATADIR = ROOTDIR / 'data' / 'raw' / 'assemblee'
DATADIR.mkdir(exist_ok=True)
OUTDIR = ROOTDIR / 'data' / 'interim' / 'assemblee'
OUTDIR.mkdir(exist_ok=True)


def save_test_dataloaders():
    discours = pd.read_parquet(OUTDIR / 'discours_assemblee.parquet')
    dls = TextDataLoaders.from_df(discours, text_col='speech', label_col='label')
    torch.save(dls, OUTDIR / 'discours_assemblee_class_databunch.pkl')
    print(dls.one_batch())


def fetch_pipeline():

    fetcher = GouvFetcher(url='https://echanges.dila.gouv.fr/OPENDATA/Debats/AN/{}/',
                          datadir=DATADIR,
                          n_workers=20)
    for year in range(2017, 2021):
        fetcher.fetch_and_save(year)


def parse_pipeline():
    all_files = list(DATADIR.glob('CRI_*.xml'))

    parser = CriParser()
    out = parallel(parser.parse_file, all_files, progress=True, n_workers=30)

    df = pd.DataFrame(chain(*out))
    print('inital_shape', df.shape)
    df.speech = df.speech.str.strip()
    df.drop(df[df.speech.isnull()|(df.speech=='')].index, inplace=True)
    print('nonull_speech', df.shape)
    df.orateur = df.orateur.fillna(method='ffill')
    df.drop(df[df.orateur.isnull()].index, inplace=True)
    print('nonull_orateur', df.shape)
    df['dateseance'] = pd.to_datetime(df.dateseance)
    df['is_valid'] = np.int8(df.dateseance >= pd.Timestamp('2020-08-01'))
    df['orateur'] = df.orateur.str.replace('.', '')

    df['label'] = df.orateur.apply(lambda x: (x.split() + [''])[0])
    tops = df['label'].value_counts()
    tops = tops[tops < 10]
    df.loc[df.label.isin(tops.index), 'label'] = ''
    tops = df.orateur.value_counts()

    print(df.head())
    print(df.isnull().mean(0))
    df.to_parquet(OUTDIR / 'discours_assemblee.parquet')


class GouvFetcher(object):
    def __init__(self, url, datadir, overwrite=False, n_workers=20):
        self.url = url
        self.datadir = datadir
        self.overwrite = overwrite
        self.n_workers = n_workers

        datadir.mkdir(exist_ok=True)

    def list_files(self, year):
        url = self.url.format(year)
        r = requests.get(url)
        assert r.status_code == 200

        soup = BeautifulSoup(r.text, 'html.parser')
        links = [url + x.text for x in soup.find_all('a', href=lambda s: '.taz')]
        return links

    def filter_links(self, links):
        return filter(lambda x: x.endswith('.taz'), links)

    def fetch_and_save(self, year):
        fns = list(self.filter_links(self.list_files(year)))
        dests = parallel(self.save, fns, n_workers=self.n_workers, progress=True, threadpool=True)
        parallel(self.extract, list(dests), n_workers=self.n_workers, progress=True)

    def save(self, fn):
        dest = Path(self.datadir) / os.path.basename(fn)
        if dest.exists() and not self.overwrite:
            return dest

        r = requests.get(fn)
        assert r.status_code == 200

        with open(dest, 'wb') as f:
            f.write(r.content)
        return dest

    def extract(self, fn):
        with tarfile.open(fn) as tf:
            tf.extractall(fn.parents[0])

        with tarfile.open(fn.with_suffix('.tar')) as tf:
            tf.extractall(fn.parents[0])


class CriParser(object):
    def parse(self, soup):
        meta = self.metadonnees(soup)
        comptes = self.compterendus(soup)
        return comptes

    def metadonnees(self, soup):
        meta = soup.find('metadonnees')
        if not meta:
            return {}
        return soup_elements(meta, [x.name for x in meta.find_all()])

    def compterendus(self, soup):
        return list(chain(*[self.single_compterendu(x) for x in soup.find_all('compterendu')]))

    def single_compterendu(self, compte):
        meta = self.metadonnees(compte)

        all_sections = filter(lambda x: x.__class__.__name__ == 'Tag',
                              list(chain(*compte.find_all('section'))))
        para = list(chain(*[s.find_all('para') or [] for s in all_sections]))
        speech = [{'dateseance': meta['dateseance'], **self.speech_info(p)} for p in para]
        return speech

    def speech_info(self, para):
        orateur = para.find('orateur')
        orateur = orateur.find('nom').text.split(',')[0] if orateur else None
        return {'speech': ' '.join(para.find_all(text=True, recursive=False)),
                'orateur': orateur}

    def parse_file(self, fn):
        with open(fn, encoding='utf-8') as f:
            cri = BeautifulSoup(f.read(), features="html.parser")
        return self.parse(cri)
