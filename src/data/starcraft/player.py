import requests
from sqlalchemy import desc
from src.app import create_app
from src.app.models import save_multiple

from ...utils.utils import check_request_status
from .blizzard import ID_REGION, base_url, generate_header
from .models import Ladder, Player


def save_grandmasters(config: str = "DEV", header: dict = None):
    app = create_app(config)
    with app.app_context():
        ladders = Ladder.query.all()
        for ladder in ladders:
            print(ladder)
            players = fetch_ladder_players(ladder, header)
            [save_player_snapshot(p) for p in players]


def fetch_ladder_players(ladder: Ladder, header: dict) -> list[Player]:
    """
    Return the list of players competing in a ladder.
    """
    header = generate_header(header)
    url = "{}/sc2/legacy/ladder/{}/{}".format(
        base_url(ladder.region), ID_REGION.index(ladder.region), ladder.id
    )
    r = requests.get(url, headers=header)
    check_request_status(r)
    players = Player.players_from_api_output(r.json(), ladder)
    return players


def save_player_snapshot(player: Player):
    """
    Save player in database only if the current snapshot is different than the last one.
    """
    last_snapshot = player_last_snapshot(player.blizzard_id)
    if last_snapshot is None or player != last_snapshot:
        save_multiple([player])


def player_last_snapshot(blizzard_id: int) -> Player:
    return (
        Player.query.filter(Player.blizzard_id == blizzard_id)
        .order_by(desc(Player.dt))
        .first()
    )
