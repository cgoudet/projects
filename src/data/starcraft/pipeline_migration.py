import re
import sqlite3
from datetime import datetime

import joblib
import pandas as pd
from fastcore.utils import parallel
from fastprogress import progress_bar
from src import ROOTDIR
from src.app import create_app
from src.data.starcraft.ladder import save_grandmaster_ladders
from src.data.starcraft.matches import save_match_api_output
from src.data.starcraft.models import Player
from src.data.starcraft.player import player_last_snapshot, save_player_snapshot


def migrate_ladder():
    print("ladder")
    save_grandmaster_ladders()


def migrate_players():
    print("players")
    con = sqlite3.connect(ROOTDIR / "data" / "raw" / "starcraft" / "starcraft.db")
    players = pd.read_sql("select * from players", con).sort_values("dt")
    players = [
        Player(
            blizzard_id=p["player_id"],
            name=p["name"],
            clan=p["clan"],
            race=p["race"],
            ladder_id=p["ladder_id"],
            profile=p["profile"],
            dt=datetime.fromtimestamp(p["dt"]),
        )
        for _, p in players.iterrows()
    ]
    [save_player_snapshot(p) for p in players]


from collections import defaultdict


def migrate_matches():
    fns = sorted(
        (ROOTDIR / "data" / "raw" / "starcraft" / "matches").glob(
            "player_matches*.joblib"
        )
    )
    df = pd.DataFrame(
        {
            "fns": fns,
            "blizzard_id": [
                re.match(r"player_matches_([0-9]*)_.*", fn.stem).group(1) for fn in fns
            ],
        }
    )
    d = defaultdict(list)
    [d[id].append(fn) for fn, id in df.values]
    l = [v for k, v in d.items()]
    print(len(l))
    parallel(migrate_player_files, l, n_workers=20, progress=True, threadpool=True)
    # [migrate_match_file(fn) for fn in progress_bar(fns)]


def migrate_player_files(files):
    files = sorted(files)
    app = create_app("PROD")
    with app.app_context():
        blizzard_id = re.match(r"player_matches_([0-9]*)_.*", files[0].stem).group(1)
        player = player_last_snapshot(blizzard_id)
        [save_match_api_output(joblib.load(fn), player) for fn in files]


def migrate_match_file(fn):
    blizzard_id = re.match(r"player_matches_([0-9]*)_.*", fn.stem).group(1)
    player = player_last_snapshot(blizzard_id)
    save_match_api_output(joblib.load(fn), player)


def main():
    # config = "PROD"
    # app = create_app(config)
    # with app.app_context():
    # migrate_ladder()
    # migrate_players()
    migrate_matches()


if __name__ == "__main__":
    main()
