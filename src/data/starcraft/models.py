import datetime
import json
import re

from sqlalchemy import (
    Column,
    DateTime,
    ForeignKey,
    Integer,
    SmallInteger,
    String,
    UniqueConstraint,
)

from ... import ROOTDIR
from ...app import DB

REFDIR = ROOTDIR / "references" / "starcraft"
MAP_NAMES = json.load((REFDIR / "map_names.json").open())
MATCH_TYPES = json.load((REFDIR / "match_types.json").open())
MATCH_DECISIONS = json.load((REFDIR / "decisions.json").open())


class Ladder(DB.Model):
    __tablename__ = "ladder"
    __bind_key__ = "starcraft"

    id = Column(Integer, primary_key=True)
    region = Column(String(2), nullable=False)
    league = Column(SmallInteger, nullable=False)
    season = Column(SmallInteger, nullable=False)
    queue = Column(SmallInteger, nullable=False)
    team_type = Column(SmallInteger, nullable=False)
    players = DB.relationship("Player", backref="ladder", lazy="dynamic")

    @classmethod
    def from_api_output(cls, api_output: dict, region: str):
        return cls(
            id=api_output["tier"][0]["division"][0]["ladder_id"],
            region=region,
            league=api_output["key"]["league_id"],
            season=api_output["key"]["season_id"],
            queue=api_output["key"]["queue_id"],
            team_type=api_output["key"]["team_type"],
        )


_BARCODE = re.compile("^[lI]{1,}$")


class Player(DB.Model):
    __tablename__ = "player"
    __bind_key__ = "starcraft"

    id = Column(Integer, primary_key=True)
    blizzard_id = Column(Integer, nullable=False)
    name = Column(String(64))
    clan = Column(String(64))
    race = Column(String(4))
    profile = Column(String, nullable=False)
    ladder_id = Column(Integer, ForeignKey("ladder.id"), nullable=True)
    dt = Column(DateTime, default=datetime.datetime.utcnow)

    @classmethod
    def from_api_output(clf, player: dict, ladder: Ladder = None):
        character = player["character"]
        name = (
            character["displayName"]
            if not re.match(_BARCODE, character["displayName"])
            else ""
        )
        return clf(
            blizzard_id=int(character["id"]),
            clan=character["clanName"] or None,
            name=name,
            race=player["favoriteRaceP1"][0],
            profile=character["profilePath"],
            ladder_id=None if ladder is None else ladder.id,
        )

    @classmethod
    def players_from_api_output(cls, content: dict, ladder: Ladder = None):
        return [
            cls.from_api_output(player, ladder) for player in content["ladderMembers"]
        ]

    def __eq__(self, other):
        to_compare = ["blizzard_id", "name", "clan", "race", "profile", "ladder_id"]
        return all(getattr(self, att) == getattr(other, att) for att in to_compare)


class Match(DB.Model):
    __tablename__ = "match"
    __bind_key__ = "starcraft"
    id = Column(Integer, primary_key=True)
    map_id = Column(Integer, nullable=False)
    type_id = Column(Integer, nullable=False)
    player_id = Column(Integer, nullable=False)
    decision = Column(Integer, nullable=False)
    dt = Column(DateTime, nullable=False)

    __table_args__ = (
        UniqueConstraint("map_id", "player_id", "dt", name="_unique_player_match"),
    )

    @classmethod
    def from_api_output(cls, content: dict, player: Player):
        return cls(
            map_id=Map.query.filter(Map.name == content["map"]).first().id,
            player_id=player.blizzard_id,
            decision=MATCH_DECISIONS[content["decision"]],
            type_id=MATCH_TYPES[content["type"]],
            dt=datetime.datetime.fromtimestamp(content["date"]),
        )

    @classmethod
    def matches_from_api_output(cls, raw_matches: dict, player: Player):
        """
        Create a list of Match from the raw output of the Blizzard match API

        :raw_matches:
        Json output of the API

        :player:
        Player instance whose matches belong
        """
        if not raw_matches:
            return []
        return [cls.from_api_output(match, player) for match in raw_matches["matches"]]


class Map(DB.Model):
    __tablename__ = "map"
    __bind_key__ = "starcraft"
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)

    @classmethod
    def from_matches_api_output(cls, raw_matches: dict):
        """
        Create a list of maps from the output of the Blizzart Match API
        """
        return cls(name=raw_matches["map"])
