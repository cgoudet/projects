import numpy as np
import requests
from fastcore.utils import parallel
from src.app import create_app
from src.app.models import save_multiple
from src.data.starcraft.blizzard import ID_REGION, base_url, generate_header
from src.data.starcraft.models import Map, Match, Player


def save_all_players_matches(config: str, n_workers: int = 10, header: dict = None):
    app = create_app(config)
    with app.app_context():
        players = Player.query.all()
        players = list({p.profile: p for p in players}.values())
        print("num players", len(players))
    # players = np.array_split(players, n_workers * 2)
    parallel(
        # save_multiple_players_matches,
        save_player_matches,
        players,
        n_workers=5,
        progress=True,
        header=header,
        config=config,
        threadpool=True,
    )


def save_multiple_players_matches(players: list[Player], config: str, header=None):
    app = create_app(config)
    with app.app_context():
        [save_player_matches(player, header=header) for player in players]


def save_player_matches(player: Player, header: dict = None, config=None):
    app = create_app(config)
    with app.app_context():
        raw_matches = fetch_matches(player, header)
        save_match_api_output(raw_matches, player)


def fetch_matches(player: Player, header: dict = None):
    header = generate_header(header)
    region = ID_REGION[int(player.profile[9])]
    url = base_url(region) + f"/sc2/legacy{player.profile}/matches"
    try:
        response = requests.get(url, headers=header)
        response.raise_for_status()
        return response.json()
    except requests.exceptions.HTTPError as excinfo:
        print(excinfo)
        return {"matches": []}


def save_match_api_output(raw_matches: dict, player: Player):
    """Save match and map to database"""
    update_map_pool(raw_matches)
    matches = Match.matches_from_api_output(raw_matches, player)
    save_multiple(matches)


def update_map_pool(raw_matches: dict):
    """
    Identify all maps from matches and update the database with unknown maps.

    :raw_matches:
    Raw output of the blizzard match api
    """
    if not raw_matches:
        return

    maps = [Map.from_matches_api_output(match) for match in raw_matches["matches"]]
    save_multiple(maps)
