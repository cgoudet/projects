import requests

from ...utils.utils import check_request_status

ID_REGION = ["", "us", "eu", "kr", "", "cn"]


def base_url(region: str) -> str:
    """
    Define the url pattern to blizzard API according to region
    """
    if region == "cn":
        return "https://gateway.battlenet.com.cn"
    return "https://{}.api.blizzard.com".format(region)


def generate_header(header=None):
    if header is not None:
        return header
    r = requests.post(
        "https://us.battle.net/oauth/token",
        data=dict(grant_type="client_credentials"),
        auth=("8f3e9d2accc54ab8938d159f675bf09c", "VHyK1bg8Zh7zBHNAlWfRyBVIsedWsjnS"),
    )
    check_request_status(r)
    header = {"Authorization": "Bearer " + r.json()["access_token"]}
    return header
