import requests

from ...app.models import save_multiple
from ...utils.utils import check_request_status
from .blizzard import ID_REGION, base_url, generate_header
from .models import Ladder


def save_grandmaster_ladders(header: dict = None):
    """
    Fetch grandmaster ladders info from all regions and save to database
    """
    regions = [x for x in ID_REGION if x]
    [save_grandmaster_ladders_from_region(r, header=header) for r in regions]


def save_grandmaster_ladders_from_region(region: str, header: dict = None):
    """
    Save the grandmaster ladder of a specific regions

    """
    raw_ladder = read_from_api(region, header=header)
    ladder = Ladder.from_api_output(raw_ladder, region)
    save_multiple([ladder])


def read_from_api(
    region: str,
    header: dict = None,
    season_id: int = 46,  # 2021
    queue_id: int = 201,
    team_type: int = 0,
    league_id: int = 6,  # grandmaster
) -> dict:
    """
    url : /data/sc2/league/{seasonId}/{queueId}/{teamType}/{leagueId}
    """
    header = generate_header(header)
    endpoint = f"/data/sc2/league/{season_id}/{queue_id}/{team_type}/{league_id}"

    url = base_url(region) + endpoint
    r = requests.get(url, headers=header)
    check_request_status(r)
    return r.json()
