import time

from src.data.starcraft.blizzard import generate_header
from src.data.starcraft.matches import save_all_players_matches
from src.data.starcraft.player import save_grandmasters


def pipeline():
    header = generate_header()
    config = "PROD"
    save_grandmasters(config, header)
    save_all_players_matches(config, n_workers=10, header=header)


if __name__ == "__main__":
    start = time.time()
    pipeline()
    end = time.time()
    print(end - start)
