from PIL import Image

from . import *


def balance_square_img(imgs, base=5):
    """Transforme les images 5*n_vars en une image plus carrée.
    """
    assert imgs.shape[0] % base == 0
    n_vars = imgs.shape[1]*base
    height = int(base * (np.sqrt(n_vars)//base))
    width = int(np.ceil(n_vars/height))
    n_empty_cols = int((width*height-n_vars)/base)

    z = np.zeros((len(imgs), n_empty_cols))
    imgs = np.concatenate((imgs, z), axis=1)
    return imgs.reshape((-1, height, width))


def add_missing_bilans(df, n_years=5):
    """Ajoute les bilans non publiés à 0"""
    g = df.groupby('entnum').agg({'annee_dtclo': np.max}).reset_index()
    g.columns = ['entnum', 'max_year']
    df = pd.merge(df, g, on='entnum', how='left')
    df['i_year'] = df['max_year'] - df['annee_dtclo']
    df.drop(df[df['i_year'] >= n_years].index, inplace=True)

    empty_df = pd.DataFrame(product(g.entnum, np.arange(n_years)), columns=['entnum', 'i_year'])
    assert len(empty_df) == len(g)*n_years

    comb = pd.concat([df, empty_df], axis=0, ignore_index=True)
    comb.drop_duplicates(['entnum', 'i_year'], inplace=True)
    assert len(empty_df) == len(comb)

    comb.sort_values(['entnum', 'i_year'], inplace=True)
    comb.fillna(0, inplace=True)
    return comb


def values_to_image(imgs, relat_log=0, background=0):
    """Transforme un 3-T en image 4-T

    Keyword Arguments:
    imgs -- 3D array
    relat_log -- apply a log transformation to rescaled values.
        Only values within the order of magnitude of relat_log are considered.
    background -- apply white (1) or black (0) for null values.
    """
    assert background in [0, 1]
    maxes = np.abs(imgs).max(axis=(1, 2)).reshape((-1, 1, 1))
    maxes = np.where(maxes == 0, 1, maxes)
    imgs = imgs / maxes
    imgs = imgs.reshape(list(imgs.shape)+[1])
    if relat_log > 0:
        imgs = log_values(imgs*pow(10, relat_log), errors='sign')/relat_log

    red = np.where(imgs < 0, -imgs, 0)
    green = np.where(imgs > 0, imgs, 0)
    blue = np.zeros(imgs.shape)
    if background == 1:
        red, blue, green = [1-x for x in (red, blue, green)]

    imgs = np.concatenate([red, green, blue], axis=3)
    imgs = np.uint8(np.floor(imgs*255))
    return imgs


def multiraw_matrix_to_image(m, target, savedir, relat_log=0, background=0):
    savedir.mkdir(exist_ok=True, parents=True)
    imgs = values_to_image(m, relat_log=relat_log, background=background)

    for i, ((entnum, target), img) in enumerate(zip(target.values, imgs)):
        if i % 100000 == 0:
            print(f'{i//1000}k')

        stdev = img.std()
        if np.isnan(stdev) or (stdev == 0):
            print(f'drop empty image : {entnum}')
            continue

        im = Image.fromarray(img)
        im.save(savedir / f'im_{entnum}_{int(target)}.png')


def pipeline_multiraw():

    for year in [2017, 2018]:
        multiraw, target = get_multiraw_matrix(year, n_years=5)
        savedir = DATADIR / f'processed/im_{year}_multiraw5_black_log0'
        multiraw_matrix_to_image(multiraw, target, savedir, relat_log=0, background=0)
        savedir = DATADIR / f'processed/im_{year}_multiraw5_black_log4'
        multiraw_matrix_to_image(multiraw, target, savedir, relat_log=4, background=0)
        savedir = DATADIR / f'processed/im_{year}_multiraw5_white_log4'
        multiraw_matrix_to_image(multiraw, target, savedir, relat_log=4, background=1)


def get_multiraw_matrix(year, n_years):
    agregs = list(np.loadtxt(ROOTDIR / 'references/agregats_bilan.txt', dtype=str))
    to_read = agregs + ['entnum', 'bi_dtclo', 'defaillance_1a']

    df = pd.read_parquet(
        next((DATADIR / f'raw/one_bil_per_year_{year}').glob('*.parquet')), columns=to_read)
    df['annee_dtclo'] = df.bi_dtclo.dt.year.astype(np.int16)

    entnum_def = (df[['entnum', 'defaillance_1a']]
                  .drop_duplicates(subset=['entnum'])
                  .sort_values('entnum'))

    comb = add_missing_bilans(df, n_years)

    imgs = balance_square_img(np.float32(comb[agregs]))
    return imgs, entnum_def
