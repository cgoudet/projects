import folium
import geopandas as gpd
import pandas as pd

from src import *


def read_geofile(level):
    DATADIR = ROOTDIR / 'data' / 'interim'

    if level == 'dep':
        geofile = (gpd.read_file(DATADIR/'departements-version-simplifiee.geojson')
                   .rename(columns={'code': 'dep'}))
        geofile['dep'] = geofile['dep'].str.replace('[AB]', '0', regex=True).astype(int)
    else:
        raise RuntimeError('Unknown level : ' + level)
    return geofile


def color_map(df, level, color_col, max_scale=None, min_scale=None, title=''):
    max_scale = ifnone(max_scale, df[color_col].max())
    min_scale = ifnone(min_scale, df[color_col].min())
    scale = np.linspace(min_scale, max_scale, 10)

    return folium.Choropleth(
        geo_data=df,
        name=title,
        data=df,
        columns=[level, color_col],
        key_on=f"feature.properties.{level}",
        fill_color='BuPu',
        threshold_scale=scale,
        smooth_factor=0
    )


def generate_tooltip(df, metrics=[]):
    def style_function(x): return {'fillColor': '#ffffff',
                                   'color': '#000000',
                                   'fillOpacity': 0.1,
                                   'weight': 0.1}

    def highlight_function(x): return {'fillColor': '#000000',
                                       'color': '#000000',
                                       'fillOpacity': 0.10,
                                       'weight': 0.1}

    fields, aliases = zip(*metrics) if metrics else ([], [])
    return folium.features.GeoJson(
        data=df,
        style_function=style_function,
        control=False,
        highlight_function=highlight_function,
        tooltip=folium.features.GeoJsonTooltip(
            fields=fields,
            aliases=aliases,
            style=("background-color: white; color: #333333; font-family: arial; font-size: 12px; padding: 10px;")
        )
    )


def generate_map(df, level='dep', color_col='defaillance_1a', cloro_opt={}, tooltip_opt={},
                 savename=None):
    geofile = read_geofile(level)
    merged = pd.merge(geofile, df, on=level, how='left')

    center_france = (46.539758, 2.430331)
    full_map = folium.Map(location=center_france, zoom_start=6)
    folium.TileLayer('CartoDB positron', name="Light Map", control=False).add_to(full_map)
    full_map.add_child(color_map(merged, level, color_col, **cloro_opt))

    tooltip = generate_tooltip(merged, **tooltip_opt)
    full_map.add_child(tooltip)
    full_map.keep_in_front(tooltip)
    folium.LayerControl().add_to(full_map)

    if savename:
        datadir = ROOTDIR / 'data' / 'interim' / 'Carte_geo'
        fn = datadir/f'{savename}.html'
        full_map.save(str(fn))

    return full_map
