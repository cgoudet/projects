import matplotlib

from . import *


def topn(values, rank):
    """Binary classification of values with respect to rank.

    Keyword arguments:
    values -- data to classify
    rank -- lowest rank to positively classify.
    If an integer, the function selects all the values equal or higher than the
    rank'th highest values.
    If a float, iteratively selects the values until the cumulative sum of density reaches rank.
    """
    if not len(values):
        return []

    svalues = np.sort(values)[::-1]
    if isinstance(rank, int):
        threshold = svalues[rank - 1]
    else:
        cumulative = np.cumsum(svalues) / svalues.sum()
        threshold = svalues[cumulative >= rank][0]

    return np.where(values >= threshold, 1, 0)


def default_ax_params(ax):
    """Define default parameters for figures."""
    ax.xaxis.label.set_size(10)
    ax.yaxis.label.set_size(10)
    # ax.tick_params(axis='x', labelsize=14)
    # ax.tick_params(axis='y', labelsize=14)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)


class Plotter(object):
    """Unique API for default plotting."""

    def __init__(self, kind, annot=False, annot_pct=True, colors=None, keepn=None,
                 topn=None, bins=None, group=None, xlabel='', ylabel=''):
        """
        Interface for standard plotting.

        Keyword arguments:
        kind -- the shape of the plot
        annot -- writes the value on top of datum
        annot_pct -- writes the relative weight of each datum as annotation
        colors -- set of clors to use for graph.
        group -- group data into categories for colors and annotation
        bins -- histogram bins
        keepn -- Keep only the n largest labels an set the rest to 'Autres'
        xlabel -- title of the x axis
        ylabel -- title of the y axis
        """
        self._set_kind(kind)
        self.annot, self.annot_pct = bool(annot), bool(annot_pct)
        self.colors = colors or 'cornflowerblue'
        self.topn = topn
        self.keepn = keepn
        self.bins = bins
        self.group = group

        self.xlabel = xlabel
        self.ylabel = ylabel

    def _set_kind(self, kind):
        allowed = ['bar', 'barh', 'hist']
        if kind not in allowed:
            raise RuntimeError('Unknown kin')
        self.kind = kind

    def plot(self, values, labels=None, savename=None, colors=None,
             title='', xlabel='', ylabel='',
             bins=None,
             group=None,
             show=False):
        """Draw and save plot.

        Keyword arguments:
        values -- data to plot
        labels -- corresponding label for bar graph
        savename -- name (with extension) of the exported file
        colors -- set of colors to use for the graph. Override class base color.
        title --
        xlabel --
        ylabel --
        show -- wether to show to created plot
        bins -- histogram bins
        group -- group data into categories for colors and annotation
        """
        fig, ax = plt.subplots()
        colors = colors or self.colors

        if self.kind.startswith('bar') and self.keepn:
            values, labels = self._keepn(values, labels)

        if self.kind == 'bar':
            labels = [str(x) for x in labels]
            ax.bar(labels, values, color=colors)
        elif self.kind == 'barh':
            labels = [str(x) for x in labels]
            ax.barh(labels, values, color=colors)
            ax.invert_yaxis()
        elif self.kind == 'hist':
            ax.hist(values, bins=bins if bins is not None else self.bins)
        else:
            raise NotImplementedError()

        group = self.group if group is None else group
        colors = self._set_color(ax, colors, group)
        self._set_bin_color(ax, colors, group)

        if self.annot:
            self._annotation(ax, groupby_color=group is not None)

        if self._is_vertical() and self.annot:
            bottom, top = ax.get_ylim()
            ax.set_ylim(top=top * 1.05)

        ax.set_title(title, loc='left')
        ax.set_xlabel(xlabel or self.xlabel)
        ax.set_ylabel(ylabel or self.ylabel)
        default_ax_params(ax)
        plt.tight_layout()

        if savename:
            fig.savefig(savename, bbox_inches='tight')
        if show:
            plt.show()
        return ax

    def _keepn(self, values, labels):
        if isinstance(self.keepn, int) and self.keepn < len(values):
            extra_values = values[self.keepn:].sum()
            values = values[:self.keepn + 1]
            values[-1] = extra_values

            labels = labels[:self.keepn + 1]
            labels[-1] = 'Autres'

        return values, labels

    def _is_vertical(self):
        return self.kind in ['bar', 'hist']

    def _values(self, ax):
        return [p.get_height() if self._is_vertical() else p.get_width()
                for p in ax.patches]

    def _annotation(self, ax, groupby_color=False):
        values = self._values(ax)
        total = sum(values)
        shift = max(values) / 50
        if not groupby_color:
            self._annotation_all(ax, shift, total)
        else:
            self._annotation_group(ax, shift, total)

    def _patch_annot_info(self, patch, shift, total):
        is_vertical = self._is_vertical()
        if is_vertical:
            xpos = patch.get_x() + patch.get_width() / 2
            ypos = patch.get_height() + shift
        else:
            xpos = patch.get_width() + shift
            ypos = patch.get_y() + patch.get_height() / 2

        val = patch.get_height() if is_vertical else patch.get_width()
        if self.annot_pct:
            val *= 100 / total

        color = patch.get_facecolor()
        return xpos, ypos, val, color

    def _add_annotation(self, ax, xpos, ypos, val, color):
        is_vertical = self._is_vertical()
        text = ('{:.1f}%' if self.annot_pct else '{:.2e}').format(val)
        ax.text(xpos, ypos,
                text,
                fontsize=15,
                ha='center' if is_vertical else 'left',
                va='bottom' if is_vertical else 'center',
                color=color)

    def _annotation_group(self, ax, shift, total):
        is_vertical = self._is_vertical()
        groups = defaultdict(list)
        for p in ax.patches:
            groups[p.get_facecolor()].append(self._patch_annot_info(p, shift, total))

        for color, info in groups.items():
            info = np.array([l[:-1] for l in info])
            xpos = (np.median if is_vertical else np.max)(info[:, 0])
            ypos = (np.max if is_vertical else np.median)(info[:, 1])
            val = np.sum(info[:, 2])
            self._add_annotation(ax, xpos, ypos, val, color)

    def _annotation_all(self, ax, shift, total):
        for patch in ax.patches:
            xpos, ypos, val, color = self._patch_annot_info(patch, shift, total)
            self._add_annotation(ax, xpos, ypos, val, color)

    def _set_color(self, ax, colors, groups=None):
        if colors is None or not colors:
            colors = ['cornflowerblue']
        elif isinstance(colors, str):
            colors = [colors]

        if self.topn:
            if len(colors) < 2:
                colors.append('lightgrey')
            return np.array(colors)

        if groups is not None:
            if len(colors) < max(groups):
                colors += ['lightgrey'] * (max(groups) - len(colors))
            return np.array(colors)

        if len(colors) < len(ax.patches):
            colors += [colors[-1]] * (len(ax.patches) - len(colors))

        if self.keepn is not None and len(ax.patches) > self.keepn:
            colors[-1] = 'lightgrey'
        return np.array(colors)

    def _set_bin_color(self, ax, colors, group=None):
        if group is None:
            group = np.arange(len(ax.patches))

        gvals = (pd.DataFrame(list(zip(self._values(ax), group)))
                 .groupby(1)
                 .agg({0: 'sum'})
                 .values.T[0])

        if self.topn:
            to_top = gvals
            if self.keepn:
                to_top = gvals[:-1]
            is_topn = topn(to_top, self.topn)
            if self.keepn:
                is_topn = np.array(list(is_topn)+[0])
            group = 1-is_topn[group]

        colors = colors[group]
        for p, c in zip(ax.patches, colors):
            p.set_facecolor(c)


class MultiPlotter(object):
    def __init__(self, limit_cat=30, adapt_types=True):
        """
        Base class to draw 2D representations.

        Keyword Arguments:
        limit_cat -- Maximum number of unique values for int and float to be considered categorical.
        """
        self.limit_cat = limit_cat
        self.adapt_types = adapt_types

    def _var_type(self, df, col):
        coltype = df[col].dtype

        nunique = df[col].nunique()
        if nunique == 1:
            return None
        if coltype.name in ['category', 'object']:
            if nunique == 2:
                return 'binary'
            else:
                return 'cat'

        if coltype.name == 'datetime64[ns]':
            return 'time'

        if self.adapt_types and df[col].nunique() < self.limit_cat:
            if nunique == 2:
                return 'binary'
            else:
                return 'cat'

        return 'float'

    def plot(self, df, featcol, tarcol=None, savename=None, show=False, title='',
             fig_opt={}, plot_opt={}):
        """Draw and save the graph.

        Keyword Arguments:
        df -- source DataFrame
        featcol -- column of variable of interest
        tarcol -- grouping variable column
        savename -- output file name including extension
        show -- wether to show the plot
        title --
        """
        fig, ax = plt.subplots()
        fig.autofmt_xdate()
        feat_type = self._var_type(df, featcol)
        target_type = self._var_type(df, tarcol) if tarcol else None

        if not (target_type or feat_type):
            raise RuntimeError('No data to draw : {} {}'.format(featcol, tarcol))

        if not feat_type and target_type:
            featcol, tarcol = tarcol, None
            feat_type, target_type = target_type, feat_type

        if feat_type == 'binary' and target_type and target_type not in ['binary', 'cat']:
            featcol, tarcol = tarcol, featcol
            feat_type, target_type = target_type, feat_type

        im = self._draw(ax, df, featcol, tarcol, feat_type, target_type, **plot_opt)

        ax.set_title(title, loc='left')

        self._labels(ax, featcol, tarcol, feat_type, target_type)
        self._colorbar(im, fig, ax, feat_type, target_type)
        if (feat_type != 'time'):
            ax.set_xscale(fig_opt.get('xscale', ax.get_xscale()))

        if bool(set([target_type, feat_type]) - set(['cat', 'binary', None])):
            ax.set_yscale(fig_opt.get('yscale', ax.get_yscale()))

        ax.set_xlabel(fig_opt.get('xlabel', ax.get_xlabel()))
        if not bool(set([target_type, feat_type]) - set(['cat', 'binary', None])):
            # ax.legend().set_title(fig_opt.get('xlabel', ax.get_xlabel()))
            ax.set_xlabel('')

        if set([target_type, feat_type]) == set(['float', 'cat']):
            ax.set_ylabel('')
        ax.set_ylabel(fig_opt.get('ylabel', ax.get_ylabel()))
        plt.tight_layout()

        if savename:
            fig.savefig(savename)
        if show:
            plt.show()
            return ax
        else:
            plt.close()

    def _colorbar(self, im, fig, ax, feat_type, target_type):
        cond = (set([target_type, feat_type]) == set(['float']))
        if cond:
            fig.colorbar(im, ax=ax)

    def _labels(self, ax, featcol, tarcol, feat_type, target_type):
        inverse_order = ((not target_type and feat_type in ['binary', 'cat'])
                         | (not (set([target_type, feat_type]) - set(['cat', 'binary']))))

        if set([target_type, feat_type]) == set(['float', 'binary']):
            tarcol = ''
        if inverse_order:
            ax.set_xlabel(tarcol)
            ax.set_ylabel(featcol)
        else:
            ax.set_xlabel(featcol)
            ax.set_ylabel(tarcol)
        default_ax_params(ax)

    def _draw(self, ax, df, featcol, tarcol, feat_type, target_type, **kwargs):
        default_color = 'cornflowerblue'
        cols = [featcol] + ([tarcol] if tarcol else [])
        df = df[cols].dropna().copy()
        if not len(df):
            raise RuntimeError("No data in {}".format(cols))

        param_keepn = kwargs.get('keepn', None)
        if 'keepn' in kwargs:
            del kwargs['keepn']

        im = None
        if not target_type and feat_type == 'float':
            kwargs['bins'] = kwargs.get('bins', 100)
            kwargs['color'] = kwargs.get('color', default_color)
            im = ax.hist(df[featcol].values, **kwargs)
        elif not target_type and feat_type in ['binary', 'cat']:
            df = df[featcol].astype(str).value_counts().sort_values(ascending=False)
            labels, values = df.index.astype(str).values, df.values
            colors = default_color
            if param_keepn:
                labels, values = keepn(labels, values, param_keepn)
                colors = [default_color]*(len(labels)-1) + ['lightgrey']
            ax.barh(labels, values, color=colors)
            ax.invert_yaxis()

        elif not target_type and feat_type == 'time':
            df = utils.resample_plot_datetime(df[featcol])
            kwargs['color'] = kwargs.get('color', default_color)
            df.plot(ax=ax, **kwargs)

        elif not target_type:
            raise NotImplementedError("missing type")
        elif set([target_type, feat_type]) == set(['float']):
            im = ax.hexbin(df[featcol].values, df[tarcol].values, mincnt=1, cmap='Blues',
                           edgecolors='face')
        # elif set([target_type, feat_type]) == set(['float', 'cat']):
        #     im = sns.violinplot(x=tarcol, y=featcol, data=df, ax=ax, orient='v', scale='area')
        elif not set([target_type, feat_type]) - set(['cat', 'binary']):
            kwargs['kind'] = kwargs.get('kind', 'barh')
            kwargs['stacked'] = kwargs.get('stacked', True)
            df['count'] = 1

            if param_keepn:
                g = (df.groupby(featcol).size()
                     .sort_values(ascending=False)
                     .iloc[:param_keepn].index.values)
                df = df.copy()
                df.loc[~df[featcol].isin(g), featcol] = 'Autre'
            table = pd.pivot_table(df, columns=tarcol, index=featcol,
                                   values='count', aggfunc='count', fill_value=0).astype(int)
            table.plot(ax=ax, **kwargs)
            ax.invert_yaxis()

        elif feat_type == 'float' and target_type in ['binary', 'cat']:
            cat_values = sorted(df[tarcol].unique())
            data = [df.loc[df[tarcol] == c, featcol].values for c in cat_values]
            kwargs['bins'] = kwargs.get('bins', 100)
            # kwargs['stacked'] = kwargs.get('stacked', True)
            kwargs['histtype'] = kwargs.get('histtype', 'barstacked')
            im = ax.hist(data, label=cat_values, **kwargs)
            ax.legend(title=tarcol)

        elif feat_type == 'time' and target_type in ['binary', 'cat']:
            cat_values = sorted(df[tarcol].unique())
            data = [utils.resample_plot_datetime(df.loc[df[tarcol] == c, featcol], 'M')
                    for c in cat_values]
            data = pd.concat(data, axis=1)
            data.columns = cat_values
            data.plot(ax=ax, **kwargs)
            ax.legend(title=tarcol)
            ax.set_ylabel('')

        else:
            raise NotImplementedError("unknown case")
        return im


def keepn(labels, values, keepn=15):
    if isinstance(keepn, int) and keepn < len(values):
        extra_values = values[keepn:].sum()
        values = list(values[:keepn + 1])
        values[-1] = extra_values

        labels = list(labels[:keepn + 1])
        labels[-1] = 'Autres'

    return labels, values


def heatmap(data, row_labels=None, col_labels=None, ax=None,
            cbar_kw={}, cbarlabel="", figsize=(8, 6), **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    cbar_kw
        A dictionary with arguments to `matplotlib.Figure.colorbar`.  Optional.
    cbarlabel
        The label for the colorbar.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """
    xlabel, ylabel = None, None
    if isinstance(data, pd.DataFrame):
        if not row_labels:
            row_labels = list(data.index.values)
        if not col_labels:
            col_labels = list(data.columns.values)
        xlabel = data.columns.name
        ylabel = data.index.name

    if not ax:
        fig, ax = plt.subplots(figsize=figsize)
    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)
    cbar.ax.set_ylabel(cbarlabel, rotation=-90, va="bottom")

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels or range(data.shape[1]))
    ax.set_yticklabels(row_labels or np.arange(data.shape[0]))

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    for edge, spine in ax.spines.items():
        spine.set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_xlabel(xlabel)
    ax.xaxis.set_label_position('top')
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.set_ylabel(ylabel)
    ax.tick_params(which="minor", bottom=False, left=False)
    annotate_heatmap(im, data)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=["black", "white"],
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A list or array of two color specifications.  The first is used for
        values below a threshold, the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts
