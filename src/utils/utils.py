import datetime
import logging
from logging.handlers import RotatingFileHandler

from pythonjsonlogger import jsonlogger

from . import *


def check_request_status(r):
    if r.status_code != 200:
        print(r.url)
        print(r.status_code)
        print(r.content)
        raise RuntimeError(
            str(r.status_code) + " " + r.content.decode("utf-8") + " " + r.url
        )


def get_logger(fn):
    logger = logging.getLogger("walmart")
    logger.setLevel(20)
    handler = RotatingFileHandler(fn, maxBytes=100 * 8 * 2 ** (10 * 2), backupCount=5)
    formatter = JsonFormatter(fmt="(levelname) $(message)")
    handler.setFormatter(formatter)
    handler.setLevel(20)
    logger.addHandler(handler)
    return logger


class JsonFormatter(jsonlogger.JsonFormatter, object):
    def __init__(
        self,
        fmt="%(asctime) %(name) %(processName) %(filename)  %(funcName) %(levelname) %(lineno) %(module) %(threadName) %(message)",
        datefmt="%Y-%m-%dT%H:%M:%SZ%z",
        style="%",
        extra={},
        *args,
        **kwargs,
    ):
        self._extra = extra
        jsonlogger.JsonFormatter.__init__(
            self, fmt=fmt, datefmt=datefmt, *args, **kwargs
        )

    def process_log_record(self, log_record):
        # Enforce the presence of a timestamp
        if "asctime" in log_record:
            log_record["timestamp"] = log_record["asctime"]
        else:
            log_record["timestamp"] = datetime.datetime.utcnow().strftime(
                "%Y-%m-%dT%H:%M:%S.%fZ%z"
            )

        if self._extra is not None:
            for key, value in self._extra.items():
                log_record[key] = value
        return super(JsonFormatter, self).process_log_record(log_record)


def addl(l1, l2):
    return sorted(set(l1) | set(l2))


def diffl(l1, l2):
    return sorted(set(l1) - set(l2))


def check_missing_cols(df, columns):
    missings = diffl(columns, df.columns)
    if missings:
        raise KeyError(f"Missing columns : {missings}")


def check_missings(df, columns):

    if isinstance(df, pd.DataFrame):
        check_missing_cols(df, columns)

        missings = df.loc[:, columns].isnull().mean(0)
        missings = missings[missings > 0]
        if len(missings):
            print(missings)
            raise RuntimeError("missing values")
    elif isinstance(df, pd.Seris):
        missings = df[df.isnull()]
        if len(missings):
            print(missings)
            raise RuntimeError("missing values")


def multi_replace(string, replacements):
    """Given a string and a replacement map, it returns the replaced string.

    :param str string: string to execute replacements on
    :param dict replacements: replacement dictionary {value to find: value to replace}

    :return: modified string
    :rtype: str

    This code is an adaptation of : https://gist.github.com/bgusach/a967e0587d6e01e889fd1d776c5f3729
    """
    if not len(replacements):
        return string

    # Place longer ones first to keep shorter substrings from matching where the longer ones should take place
    # For instance given the replacements {'ab': 'AB', 'abc': 'ABC'} against the string 'hey abc', it should produce
    # 'hey ABC' and not 'hey ABc'
    substrs = sorted(replacements, key=len, reverse=True)

    # Create a big OR regex that matches any of the substrings to replace
    regexp = re.compile("|".join(map(re.escape, substrs)))

    # For each match, look up the new string in the replacements
    return regexp.sub(lambda match: replacements[match.group(0)], string)


def pickle_size(model):
    with tempfile.TemporaryDirectory() as tmpdirname:
        fn = Path(tmpdirname) / "model.joblib"
        joblib.dump(model, fn)
        return fn.stat().st_size


def read_external(d, key, attr="features"):
    val = d.get(key)
    if val is None:
        return val

    if isinstance(val, list) or isinstance(val, dict):
        return val

    if not isinstance(val, str):
        raise RuntimeError("Wrong input format : {}".format(type(val)))

    if val.endswith(".joblib"):
        clf = joblib.load(Path(val))
        return getattr(clf, attr)

    if val.endswith(".txt"):
        path = Path(val)
        if not path.is_absolute():
            path = ROOTDIR / path
        return list(np.loadtxt(path, dtype=str))

    if val.endswith(".json"):
        with Path(val).open() as f:
            return json.load(f)

    raise RuntimeError("Wrong string format : {}".format(val))
