import functools
import json
import os
import re
import shutil
import sqlite3
import sys
import tempfile
import time
from collections import defaultdict
from functools import partial
from itertools import chain, product
from pathlib import Path

import category_encoders as ce
import joblib
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from fastcore.utils import ifnone, listify, parallel, store_attr
from sklearn.base import BaseEstimator, RegressorMixin, TransformerMixin

ROOTDIR = Path(__file__).parent / ".."

import shap

mpl.rcParams["axes.spines.right"] = False
mpl.rcParams["axes.spines.top"] = False

if True:
    from .utils.decorators import tracker
    from .utils.utils import *
